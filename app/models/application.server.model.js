'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Application Schema
 */
var ApplicationSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Application name',
		trim: true
	},
	contactInfo: {
		type: String
	},
	created: {
		type: Date,
		default: Date.now
	},

	message : {
		type: String
	},
	voteStatus : {   //whether or not an application has been voteStatus or not ?
		type: String,
		enum:['upvoted','downvoted','none']
	},

	viewed : {  //whether or not this application has been viewed or not ---> used to send a notification to the user letting them
				//know that someone viewed their application ? 
		type: Boolean 
	},


	//refers back to the initial user object of this application
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},

	employer:{ //holds the employer of the casting that this application is for ---> USED FOR AUTHENTICATION b/c only employer can accept stuff
		type: Schema.ObjectId,
		ref : 'User'
	},

	casting_applied_for : {type: Schema.ObjectId, ref: 'Casting'} //casting that the user applied for 

});

mongoose.model('Application', ApplicationSchema);
