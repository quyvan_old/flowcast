'use strict';

// Castings controller
// Might be better in future to place *CastingFields in a seperate controller
angular.module('castings').controller('CastingsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Castings', 'Applications', 'Pagination', 'Reviews', 'UserCheck','NotificationUtil','Notifications', 'CreateNotification', 'SelectionLists', 'CheckCharacteristics','GlobalSettings', '$window', '$sce','S3Conn', 'YoutubeHandler',
	function($scope, $stateParams, $location, Authentication, Castings,Applications, Pagination, Reviews, UserCheck,NotificationUtil,Notifications, CreateNotification, SelectionLists, CheckCharacteristics, GlobalSettings, $window, $sce, S3Conn, YoutubeHandler) {
	/*Set Variables*/
		//services
		$scope.pagination = Pagination;
		$scope.userCheck = UserCheck;
		$scope.globalSettings = GlobalSettings;
		$scope.checkCharacteristics = CheckCharacteristics;
		$scope.authentication = Authentication;
		$scope.selectionLists = SelectionLists;
		$scope.user = Authentication.user;

		//================used for updating the progress bar as the progress changes========================
		$scope.$watch(function () { return S3Conn.upload_progress(); }, function (newVal, oldVal) {
			$scope.uploadProgress = newVal;
		});
		//================================================================================================

		//temp scope variables ---> used for pic upload
	//	$scope.hasCastingPic = false; //whether or not you have a casting picture
		//$scope.casting_pic_url = false; 

		//fields
		$scope.newCasting = {languages: [],portrayableEthnicity: [], skills: []}; //workaround for tags while creating casting
		$scope.descriptionMaxLength = 1000;
		$scope.titleMaxLength = 50;
		$scope.paymentMaxLength = 20;

	/*Set Functions*/
		$scope.createNotification = function (name, user, createdBy, message) {
			CreateNotification.createNotification(name, user, createdBy, message);
		};

		$scope.create = function() {
			$scope.newCasting.progress='hiring';
			$scope.newCasting.user=$scope.authentication.user._id;
			$scope.newCasting.movie_img_url=$scope.casting_pic_url;
			var casting = new Castings ($scope.newCasting);
			// Redirect after save
			casting.$save(function(response) {
				
				if ($scope.casting_pic_url){ //if they uploaded a picture...
					$scope.upload($scope.user.userUrl + '/casting/' + response._id); //save it to the relevant casting bucket
					console.log('uploaded to bucket');
				}	


				$location.path('castings/' + response._id);




				// Clear form fields
				$scope.newCasting = {tags: {creative: false, labour: false}}; //reseting... var is a workaround for tags while creating casting
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		

		};


		$scope.closeCastingAndRefresh = function () {
				var currentCasting = Castings.get({     //use the casting service to query for the specific casting you are creating an application for
					castingId: $stateParams.castingId
				}, function (response) {
					currentCasting.progress='closed';
					currentCasting.$update(function (res) {
						$window.location.reload();
					});
				});
		};


		$scope.completeCasting = function(){
			//var employer = new Users($scope.authentication.user); //get the user that created this application ---> only a reference to their id though
			//var employee =  new Users($scope.application.user);
			//this is a good way to get the "employee" user object ---> but why can't I do USERS.get(userId) directly

				var currcasting = Castings.get({     //use the casting service to query for the specific casting you are creating an application for
					castingId: $stateParams.castingId
				}, function(response){
					var employer = currcasting.user;
					var employee = currcasting.employee; 

					currcasting.$update(function() {
						}, function(errorResponse) {
						$scope.error = errorResponse.data.message;
						}); 
					//get the casting and user... ---> then use this to create a pending review ? 
					//it is important that this is the second calls since they are redirected to the right one
					//this is kind of a hack, todo: use callback functions to properly return the result 
					//look at how response_id is used in other examples ? 

					//create notification for both employee and employer
					var message = 'Your casting is completed!' +  NotificationUtil.createHTMLStringCasting(' Click here ',response._id) + ' to view the casting';
					$scope.createNotification('Your casting has been completed',employee._id,'System',message);
					$scope.createNotification('Your casting has been completed',employer._id,'System',message);


					//create reviews
					
					$scope.tempReview = {}; //variable used to hold review for employer AND employee


					/*Function below follow this order
					1) Create review written by employee
					2) create review written by employer
					3) Update the casting variable with the newly created reviews
					4) Redirect to review written by employer...since employer currently sets when reviews are created*/
					$scope.createPendingReview(employer, employee, currcasting, 'Employer',1, 'for_employer', function (){
						$scope.createPendingReview (employee, employer, currcasting, 'Employee',1, 'for_employee', function () {
							$scope.casting.review = $scope.tempReview;
							$scope.casting.progress = 'closed';
							$scope.update();
							$location.path('reviews/'+ $scope.casting.review.for_employee);
						});
					});
				});
		};

		$scope.createPendingReview = function(userFor,userCreated,casting,role_name, notifFlag, reviewForInput,next ){
			//userCreated ---> user who is creating the review
			//userFor ---> user who the review is FOR
			//role ---> role o fthe user you are reviewing (employee/employer)
			//casting ---> casting this review is about 

			var review_title = 'Review for user '.concat(String(userFor.username));

			//inject review service and use it here to create a new review ---> then they can see it in pending reviews?
			var review = new Reviews ({
				title: review_title,   //title should be like Employee review for casting id # XYZ or Employer Review for casting XYZ
				userFor : userFor._id,
				role : role_name, 
				casting: casting._id,
				userCreated : userCreated._id, //no longer needed
				user : userCreated._id,
				completed: false,
				reviewFor: reviewForInput
			});
			review.$save(function(response) {
				//update casting with the review
				$scope.tempReview[reviewForInput] = review._id;
				//$scope.$apply();
				if (notifFlag === 1){
					//create a notification for this user as wlel
				 	//CREATE THE NOTIFICATION UPON CREATING AN APPLICATION
					var user_link = NotificationUtil.createHTMLStringUser(userFor.displayName,userFor.userUrl);  //pass in user and message 
					var review_link = NotificationUtil.createHTMLStringReview('Click here',response._id);
					var casting_link = NotificationUtil.createHTMLStringCasting('this casting',casting._id);
					var message = 'Please complete a review for  ' + user_link +  ' concerning their performance on ' + casting_link + ' . ' + review_link + ' to start the review!';
					$scope.createNotification('Review request for completed casting',userCreated._id,'System',message);

				}
				

				// Clear form fields
				$scope.name = '';
				next();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			}); 
		};

		//Apply to casting function and validation
		$scope.applyToCasting = function(){
			var applyFlag = 0; //flag that holds whether you applied to this casting already or not (0 = false, 1  =true)
			var existing_application_id; 

			//another way to do this would be for the user to have a list of all their applications 
			//only pass casting flag ---> don't really need this ?
			var all_user_applications = Applications.query({}, function(){

				if (all_user_applications.length > 0){ //check that some applications exist obviosuly 

				for (var i = 0 ; i  < all_user_applications.length; i++){
					if (all_user_applications[i].casting_applied_for === $stateParams.castingId){
						applyFlag = 1; 
						existing_application_id = all_user_applications[i]._id;
						break; 
					}
				}

}

			if (applyFlag === 1){ //already applied to casting ---> redirect to existing application 
				alert('You have already applied to this casting! Please edit your existing application');
				$location.path('applications/' + existing_application_id );
			}

			else{

				$location.path('castings/' +  $stateParams.castingId + '/apply');
				
			}

				
			});
			//put validation code in here 


		};

		// Remove existing Casting
		$scope.remove = function(casting) {
			var confirmed = confirm('Are you absolutely sure you want to delete?');   
			if (confirmed){
				if (casting) { 
					casting.$remove();
					for (var i in $scope.castings) {
						if ($scope.castings [i] === casting) {
							$scope.castings.splice(i, 1);
						}
					}
				} else { //what is this section for?? - quy
					$scope.casting.$remove(function() {
						$location.path('castings');
					});
				}
			}
		};

		// Update existing Casting then redirect
		$scope.updateAndRedirect = function() {
			var casting = $scope.casting;
			casting.$update(function() {
				$location.path('castings/' + casting._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		// Update existing Casting
		$scope.update = function() {
			var casting = $scope.casting;
			casting.$update(function() {
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Castings
		$scope.find = function(userInput) {
			if (userInput === undefined || userInput === '') {
				$scope.castings = Castings.query(function(response){ //tag is the variable name
				});

			}
			else {
				var checkList = SelectionLists.userCharacteristics;
				var queryObject = {};
				var checkListValue;
				var userFieldValue;
				for (var checkListIndex in checkList) {
					checkListValue = checkList[checkListIndex];
					userFieldValue = userInput[checkListValue];
					if (userFieldValue !== '' && userFieldValue !== undefined && userFieldValue.length !==0 ){
						queryObject[checkListValue] = userFieldValue;
					}
				}
				
				//this var allows the program to determine if the user has no characteristics entered, or it's a search all
				queryObject.filters = 'filter'; //not using boolean because it turns into string

				$scope.castings = Castings.query(queryObject, function(response){ //tag is the variable name
				});
			}
		};

		// Find existing Casting
		$scope.findOne = function() {
			$scope.casting = Castings.get({ 
				castingId: $stateParams.castingId
			}, function () {
				console.log('casting is:');
				console.log($scope.casting);
				if ($scope.casting.youtubeUrl) {
					$scope.youtubeUrlEmbed = 'https://www.youtube.com/embed/' + YoutubeHandler.getYoutubeUrlId($scope.casting.youtubeUrl);
				}
				//$scope.casting.description = $sce.trustAsHtml($scope.casting.description);
			});
		};

		//Find casting only if user is authorized, else redirect
		$scope.ifNotAuthorizedRedirectFindOne = function () {
			$scope.casting = Castings.get({ castingId: $stateParams.castingId}, function(response) {
				UserCheck.redirectIfNotAuthorizedUser($scope.authentication.user, response.user);
			});
		};

		//Find castings created by user
		$scope.findMyCreatedCastings = function(progressInput) {
			if (progressInput === 'all') {
				progressInput='';
			}
			$scope.castings = Castings.query( {userId: $scope.authentication.user._id, progress: progressInput }, function(response){ //tag is the variable name
			});
		};


		//todo: should make this method way more modular, and add functionality through callbacks functions

		$scope.removePicture = function(pic_url,profile_flag){
			console.log('called the remove picture function!');
			console.log($scope.user.profilePicUrl);

			if (pic_url){
				console.log('removing picture from bucket...');

				//extract the actual filename ?
		   		var last_slash = pic_url.lastIndexOf('\/');
		   		//console.log(last_slash);
		   		var filename = pic_url.substr(last_slash+1);
		   		//console.log(filename);	
			    S3Conn.removeObject(filename,$scope.user.userUrl, function(err, data) {
	  	
	  				if (err) {
	  					console.log(err, err.stack); // an error occurred
	  				}
	  				else     // successful response
	  				{
	  					console.log(data); 
	  					if ($scope.casting){ //means a casting already exists ---> this is bad b/c we are reusing the same controller?
	  						$scope.casting.movie_img_url  = null;
	  						$scope.update(); //update the casting after this 
	  					}

	  					else{
	  						$scope.casting_pic_url = null;
	  						//update is done automatically 
	  					}


	  				}
			}); 

			}

			else{
				alert('You do not even have a picture to remove!');
			}
		};

		$scope.upload = function(upload_path){
			

			console.log('called file upload function');
			console.log('upload path is ' + upload_path);
			if ($scope.file){ //make sure a file is selected


				//================validation checks=============//
				if ( String($scope.file.type).indexOf('image') === -1){
					alert('Please enter an image file');
					console.log($scope.file.type);
				//	console.log(String($scope.file.type).indexOf('image'));
					return false;
				}

				if ($scope.file.size > 2117152){
					alert('Sorry, file must be under 2mb');
					return false; 
				}

				//==============end validation checks=============/
					
				S3Conn.putObject($scope.file,upload_path,function(err, data, public_url){
					//should return the public url to the file ? 
					 console.log('finished uploading picture...');
				      if(err) {
				        // There Was An Error With Your S3 Config
				        alert(err.message);
				        return false;
				      }
				      else {
					      	console.log(data);
					      	console.log(public_url);
					      	 
					     	console.log($scope.user.userUrl);
					     	if ($scope.casting){ //update the actual casting and stuff
	  							$scope.casting.movie_img_url  = public_url;
	  							$scope.update(); //update the casting after this 
					     	}

					     	else{
					     		$scope.casting_pic_url = public_url; //casting doesnt already existing, you are creating one, use local scope
					     	}


						}
					}, function() {  $scope.$apply(); console.log('in cb function'); } ) ;
					//another way to do this would be to set the scope variable in the cb function ... but that's a bad way
					//to do it $scope.watch is easier


			}
			else{
				alert('Please select a file');
				return false; 
			}	


		};

	}
]);
