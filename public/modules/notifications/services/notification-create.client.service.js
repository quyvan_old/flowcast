'use strict';

//Notifications service used to communicate Notifications REST endpoints
angular.module('notifications').factory('CreateNotification', ['Notifications',
	function(Notifications) {
		var _this = this;
		_this.createNotification = function(name, user, createdBy, message){
			//setup the notification here
			var notification = new Notifications({
				name: name,
				user: user,
				createdBy: createdBy, //usually system
				message: message,
				seen: false
			});
			notification.$save(function(response){ //save the notification 
			});
		};



		return _this;
	}
]);
