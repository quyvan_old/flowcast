Live at gigapp.herokuapp.com

A job posting website to find models/actors based on physical characteristics and skills.

Developed by Quy Van and Mustafa Abdool.

Built on the [MEAN.js](http://meanjs.org/) boilerplate. MongoDB, Express, AngularJS, Node.js

**Core Features:**

* Create, Delete, Edit, list, and view job listings (castings module)

* Create, edit, list and view users

* Create and accept/reject applications

* notifications from system alerting about applications

* Send and receive messages

**Folder Structure:**
Follows the same structure provied by MEAN.js. It follows a horizontal structure in the backend, and a vertical structure in the front end.

* ./app - contains the backend files

    * /routes - contains the files used to handle requests

    * /controllers - contains the files used to process the logic

    * /models - contains the files with schemas used in the database (Mongoose models)

    * /views - contains the error pages and some template pages

* ./config - contains config files

* ./public - contains the frontend files

    * /dist - contains the minified assets

    * /modules - contains the AngularJS modules