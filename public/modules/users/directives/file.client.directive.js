'use strict';

angular.module('users').directive('file', [
	function() {
  return {
    restrict: 'AE',
    scope: {
      file: '@' //bind to file attribute in the html 
    },
    link: function(scope, el, attrs){ 
      //el = angular element? ---> can do jquery manipulation on it , attrs = variables u pass into the directive
      el.bind('change', function(event){ //attach listener
      //	console.log(event.target);

        //file (the input type) is an HTML DOM Object thingy --> it has the attribute "files" which is what we access
        //when we used event.target. See http://www.w3schools.com/jsref/dom_obj_fileupload.asp
        var files = event.target.files;
        var file = files[0];
        scope.file = file;
        scope.$parent.file = file;
        scope.$apply(); //check if binding values have changed ---> used to call method in controller function:?
      });
    }
  };
} ] );