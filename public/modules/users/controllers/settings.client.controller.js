'use strict';


angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'SelectionLists', 'S3Conn', 'GlobalSettings',
	function($scope, $http, $location, Users, Authentication, SelectionLists,S3Conn, GlobalSettings) {
		$scope.user = Authentication.user;
		$scope.globalSettings = GlobalSettings;
		$scope.selectionLists = SelectionLists;
		$scope.overviewMaxLength = 1000;
		$scope.profilePicUrl =  $scope.user.profilePicUrl; //'https://s3-us-west-2.amazonaws.com/gigapp-bucket/elite+daily+bio+pic.png';
		$scope.selected_picture = 'unavailable'; //the picture that the user has currently clicked on
		$scope.S3Conn = S3Conn; //s3 service to use images in our bucket
		$scope.pictureLimit = 3; //maximum number of additional pictures the user can upload

		//================used for updating the progress bar as the progress changes========================
		$scope.$watch(function () { return S3Conn.upload_progress(); }, function (newVal, oldVal) {
			$scope.uploadProgress = newVal;
		});
		//================================================================================================

		//check if user has a profile picture setup, so you can display it 
		if ($scope.profilePicUrl){
			$scope.hasProfilePic = true;
		}
		else{
			$scope.hasProfilePic = false; //used in html 
		}

		// If user is not signed in then redirect back home
		if (!$scope.user)
		{
			$location.path('/');
		}


		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
				user.$update({ noUrlUpdate : 'true'},function(response) { // do we need this  noUrlUpdate flag for something?
					$scope.success = true;
					Authentication.user = response;
					var nextPage = $scope.globalSettings.popNextPage();
					console.log(nextPage);
					if (nextPage === '' || nextPage === undefined) {
						//$location.path('/users/profile/' + user.userUrl);
						location.reload();
					}
					else {
						console.log(nextPage);
						$location.path(nextPage);
					} 
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		//as of right now, it does not return the current profile picture...used to display all the other additional images
		//used on ng-init to immediately show the additional pictures the user has uploaded 
		$scope.listAllPictures = function(){
			
		//	var pic_array = []; //array which holds hte url's of all the users pictures ? 

			S3Conn.listObjects($scope.user.userUrl,function(err,data,result){

				if (err){
					console.log(err.message);
				}

				else{ //do stuff with the picture names here ---> maybe set it to a variable in scope or whatever ? 
					var i;
					var temp = [];
					for (i = 0; i < result.length; i++){
						console.log($scope.user.profilePicUrl);
						console.log(S3Conn.aws_prefix() + result[i]);


						//won't include the profile picture in the list of pictures...maybe make this an arguement ? 
						if( (S3Conn.aws_prefix() + result[i]).replace(' ','+') !== $scope.user.profilePicUrl)
						 //result[i] =  S3Conn.aws_prefix() + result[i]; //console.log(result[i]);
							temp.push(S3Conn.aws_prefix() + result[i]);
					}
					
					$scope.picturesList = temp; //result; //update the pictures list for this user 
					console.log($scope.picturesList);
				}


			});
		};

		$scope.toggle = function(url){ //lets the user know what picture url they currently have selected
			$scope.selected_picture = url; 
			console.log('you clicked on' + url);

		};

		$scope.removePicture = function(pic_url,profile_flag){
			console.log('called the remove picture function!');
			console.log($scope.user.profilePicUrl);

			if (pic_url){
				console.log('removing picture from bucket...');

				//extract the actual filename ?
		   		var last_slash = pic_url.lastIndexOf('\/');
		   		//console.log(last_slash);
		   		var filename = pic_url.substr(last_slash+1);
		   		//console.log(filename);	
			    S3Conn.removeObject(filename,$scope.user.userUrl, function(err, data) {
	  	
	  				if (err) {
	  					console.log(err, err.stack); // an error occurred
	  				}
	  				else     // successful response
	  				{
	  					console.log(data); 

	  					if (profile_flag){ // special case if they are removing their profile picture yo
		  					$scope.user.profilePicUrl = null;
	  						$scope.hasProfilePic = false; //used in html
	  						$scope.updateUserProfile(true); //clear the profile pic variable and update						
	  					}

	  					//update the picturesList variable...should probably put the reload part in a callback?
	  					//or wahtever...should be fast enough anyway.
	  					$scope.listAllPictures(); 



	  					/*else{
	  						//remove this picture from the pictures list (profile pictures arent in the list to begin with ? )
	  						//probably don't even need this b/c the scope variable will update by itself when you call it again

	  						if ($scope.picturesList.indexOf(pic_url) > -1){
	  							var index = $scope.picturesList.indexOf(pic_url);
	  							 $scope.picturesList.splice(index, 1); //remove this guy from the array
	  							 $scope.selected_picture = null; //reset this variable
	  						}
	  					}     */


	  				}
			}); 

			}

			else{
				alert('You do not even have a profile picture to remove!');
			}
		};

		$scope.setProfilePicture = function(pic_url){

			console.log('setting a new profile pic by using an existing picture...');

	        	//remove the old picture from the current list as well		
			if ($scope.picturesList.indexOf(pic_url) > -1){
				var index = $scope.picturesList.indexOf(pic_url);
				 $scope.picturesList.splice(index, 1); //remove this guy from the array
				 $scope.selected_picture = null; //reset this variable
			}

			//delete the old profile pic (if it exists) and set the variable 
	        $scope.updateProfilePic(pic_url);  			
		};

		$scope.updateProfilePic = function(new_url){
     		if ($scope.user.profilePicUrl){
	     		var old_url = $scope.user.profilePicUrl;
	     		var last_slash = old_url.lastIndexOf('\/');
					console.log('actually changing profile picture...');
					var old_filename = $scope.user.profilePicUrl.substr(last_slash+1);
	     		S3Conn.removeObject(old_filename,$scope.user.userUrl, function (err, data) {
	     			if (err){
	     				console.log('unable to remove old profile picture');
	     			}
	     			else{
	     				console.log('successfully removed picture');

	     			}

	     		});
     		}

     		//update the existing profile pic
     		$scope.user.profilePicUrl = new_url; //'https://s3-us-west-2.amazonaws.com/gigapp-bucket/' + $scope.user.userUrl + '/' + $scope.file.name.replace(' ','+'); 
        	$scope.updateUserProfile(true); //set the variable name and update the user profile pic
        	$scope.hasProfilePic = true; //let the user know that they now have a profile pic ?
		};

		//profile_flag = whether or not this picture will be the users profile picture or just an extra one
		$scope.upload = function(profile_flag){
			

			console.log('called file upload function');
			console.log(profile_flag);
			if ($scope.file){ //make sure a file is selected


				//================validation checks=============//
				if ( String($scope.file.type).indexOf('image') === -1){
					alert('Please enter an image file');
					console.log($scope.file.type);
				//	console.log(String($scope.file.type).indexOf('image'));
					return false;
				}

				if ($scope.file.size > 2117152){
					alert('Sorry, file must be under 2mb');
					return false; 
				}


				//can check for number of additional pictures if the user is not uploading a profile picture
				console.log(profile_flag);
				if (!profile_flag){
					var num_pics = $scope.picturesList.length;
					console.log('current number of pictures is ' + num_pics);
					if (num_pics > $scope.pictureLimit){
						alert('Sorry, you have already uploaded the maximum number of extra pictures. Please delete some before continuing');
						return false;
					}
					
				} 

				//==============end validation checks=============/
					
				S3Conn.putObject($scope.file,$scope.user.userUrl,function(err, data, public_url){
					//should return the public url to the file ? 
					 console.log('finished uploading picture...');
				      if(err) {
				        // There Was An Error With Your S3 Config
				        alert(err.message);
				        return false;
				      }
				      else {
				      	console.log(data);

				        // Success!
				        //remove old profile picture 
				        //set user variable 
				        //update display picturese 

				        if (profile_flag === true){
				        	$scope.updateProfilePic(public_url);
				        }

				        else{ //in the normal case update the pictures list
				        	$scope.listAllPictures();
				        }

						}
					}, function() {  $scope.$apply(); console.log('in cb function'); } ) ;
					//another way to do this would be to set the scope variable in the cb function ... but that's a bad way
					//to do it $scope.watch is easier


			}
			else{
				alert('Please select a file');
				return false; 
			}	


		};

		$scope.fileNameChanged = function(){
			console.log('select file');
		};


		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
