'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Notification Schema
 */
var NotificationSchema = new Schema({
	name: { //title/subject of the notification
		type: String,
		default: '',
		required: 'Please fill Notification name',
		trim: true
	},
	createdBy: { type: String //name of the user/system who created this notification
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {   //user that the notification is ABOUT
		type: Schema.ObjectId,
		ref: 'User'
	},

	message:{
		type: String
	},

	seen: {
		type: Boolean
	}



});

mongoose.model('Notification', NotificationSchema);