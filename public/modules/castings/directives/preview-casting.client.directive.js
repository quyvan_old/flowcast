'use strict';

angular.module('castings').directive('previewCasting', ['UserCheck', 'CheckCharacteristics', 'Authentication', 'GlobalSettings',
	function(UserCheck, CheckCharacteristics, Authentication, GlobalSettings) {
		var directive = {};

		directive.scope = {
			casting: '=injectCasting',
			viewType: '=injectViewType',
			customTemplate: '@injectCustomTemplate'
		};

		directive.link = function (scope) {
			scope.userCheck = UserCheck;
			scope.globalSettings = GlobalSettings;
			scope.checkCharacteristics = CheckCharacteristics;
			scope.authentication = Authentication;
		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/castings/views/preview-casting.client.view.html';
		return directive;
}]);


