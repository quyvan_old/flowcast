'use strict';

//Castings service used to communicate Castings REST endpoints
angular.module('castings').factory('Castings', ['$resource',
	function($resource) {
		return $resource('castings/:castingId', { castingId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
