'use strict';

//Setting up route
angular.module('reviews').config(['$stateProvider',
	function($stateProvider) {
		// Reviews state routing
		$stateProvider.
		state('listPendingReviews', {
			url: '/reviews/pending',
			templateUrl: 'modules/reviews/views/list-reviews-pending.client.view.html'
		}).
		state('listCompletedReviews', {
			url: '/reviews/completed',
			templateUrl: 'modules/reviews/views/list-reviews-completed.client.view.html'
		}). 
		state('createReview', {
			url: '/reviews/create',
			templateUrl: 'modules/reviews/views/create-review.client.view.html'
		}).
		state('viewReview', {
			url: '/reviews/:reviewId',
			templateUrl: 'modules/reviews/views/view-review.client.view.html'
		}).
		state('editReview', {
			url: '/reviews/:reviewId/edit',
			templateUrl: 'modules/reviews/views/edit-review.client.view.html'
		});
	}
]);