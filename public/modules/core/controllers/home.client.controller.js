'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication','$location', 'Castings', 'GlobalSettings','UserCheck',
	function($scope, Authentication, $location, Castings, GlobalSettings, UserCheck) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.userCheck = UserCheck;
		$scope.globalSettings = GlobalSettings;
		$scope.mainImgUrl = $location.path() + 'images/moose.jpg' ;

	}
]);
