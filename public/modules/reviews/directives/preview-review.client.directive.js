'use strict';

angular.module('reviews').directive('previewReview', 
	function() {
		var directive = {};

		directive.scope = {
			review: '=injectReview',
			writer: '=injectWriter',
			reviewFor: '=injectReviewFor',
			customTemplate: '@injectCustomTemplate'
		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/reviews/views/preview-review.client.view.html';
		return directive;
});


