'use strict';

angular.module('users').directive('previewUser', ['UserCheck',
	function(UserCheck) {
		var directive = {};

		directive.scope = {
			user: '=injectUser',
			customTemplate: '@injectCustomTemplate'
		};

		directive.link = function (scope) {
			scope.userCheck = UserCheck;
		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/users/views/preview-user.client.view.html';
		return directive;
}]);


