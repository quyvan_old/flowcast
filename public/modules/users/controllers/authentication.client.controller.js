'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', 'Users', 'GlobalSettings',
	function($scope, $http, $location, Authentication, Users, GlobalSettings) {
		$scope.authentication = Authentication;
		$scope.globalSettings = GlobalSettings;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				var nextPage = $scope.globalSettings.popNextPage();
				// And redirect to the index page
				if (nextPage === '' || nextPage === undefined) {
					$location.path('/settings/profile');
				}
				else {
					$location.path(nextPage);
				}
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				if ($scope.authentication.user.totalLogins === undefined) {
					$scope.authentication.user.totalLogins=0;
				}
				$scope.authentication.user.totalLogins++;
				var user = new Users($scope.authentication.user);
				user.$update({ noUrlUpdate: 'true'} , function (response) {
				} , function (response) {
					$scope.error=response.data.message;

				});

				//Redirections
				var nextPage = $scope.globalSettings.popNextPage();
				if (nextPage==='/settings/profile') {
					nextPage = $scope.globalSettings.popNextPage();
				}
				if (nextPage === '' || nextPage === undefined) {
					$location.path('/');
				}
				else{
					$location.path(nextPage);
				}
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
