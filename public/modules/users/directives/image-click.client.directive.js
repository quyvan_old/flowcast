'use strict';

//works because you are not using an isolated scope? just using the parent scope instead?
//no scope varaible defined in directive? 
//to make this reusable we should probably have an isolated scope so that we aren't relying on the parent scope so much
//see http://stackoverflow.com/questions/16421111/how-to-get-access-to-a-directive-attrs-with-isolated-scope
//for more info 	 	
angular.module('users').directive('imageClick', [
	function() {
		return {
			restrict: 'AE',
			replace: 'true',	
			 link: function(scope, element, attrs) {

			 	//console.log(attrs.val);
			
				//this approach doesn't seem to work because the source actually comes from {{pic_url}} --->this is back when it was in the<div></div>
				//console.log(element[0].firstElementChild);
				//console.log(element[0].firstElementChild.getAttribute('src')); //only returns {{pic_url}}


				//add scope to watch to the image.selected tag here
				//when it changes, then you set add class to the new element, remove the class from the previous element 
				//you need to do checkign to make sure that the correct image is selectedt hough? check scope variable ?
				
				scope.$watch(function () {return scope.$parent.selected_picture;}, function(newVal, oldVal){
				/*	console.log('new val is ' + newVal);
					console.log('old val is ' + oldVal);
					console.log('this directive is about ' + attrs.val); */
					if (oldVal === attrs.val){ //remove the class from the picture because we clicked another one
						element.removeClass('selected');
					}

					if (newVal === attrs.val){
						element.addClass('selected');
					}

					}); 
			}
		};
	}
]);