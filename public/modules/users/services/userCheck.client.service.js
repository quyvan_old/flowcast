'use strict';

//this could all be done in backend... look into "roles" in user controller
angular.module('users').factory('UserCheck', ['$location', 'Authentication', 'GlobalSettings',
	function($location, Authentication, GlobalSettings) {
		var _this = this;
		_this.authentication = Authentication;
		_this.redirectIfNotLoggedIn = function (url){
			if (!_this.isSignedIn()) {
				if (url) {
					GlobalSettings.pushNextPage(url);
				}
				$location.path('/signin');
			}
		};

		//alias for redirectIfNotLoggedIn()
		_this.redirectIfNotSignedIn = function (url) {
			_this.redirectIfNotLoggedIn(url);
		};


		_this.redirectIfFirstIsNotEither = function (userOne, userTwo, userThree) {
			if (!(_this.isSameUser(userOne,userTwo) || _this.isSameUser(userOne, userThree))){
				alert('Not an authorized user');
				$location.path('/');
			}
		};

		_this.redirectIfNotSameUser = function (userOne, userTwo) {
			if (!_this.isSameUser(userOne, userTwo)){
				alert('Not an authorized user');
				$location.path('/');

			}
		};
		
		//alias for _this.redirectIfNotSameUser()
		_this.redirectIfNotAuthorizedUser = function (userOne, userTwo){
			_this.redirectIfNotSameUser(userOne, userTwo);
		};
		_this.isSignedIn = function () {
			if (!Authentication.user){
				return false;
			}
			return true;

		};
		_this.isSameUser = function (userOne, userTwo) {
			if (!userOne || !userTwo) {
				return false;
			}
			return _this.isSameUserById (userOne._id, userTwo._id);
		};

		_this.isSameUserById = function (userOneId, userTwoId) {
			if (userOneId === userTwoId){
				return true;
			}
			return false;

		};
		_this.isCurrentUser = function (user) {
			return _this.isSameUser(user, _this.authentication.user);

		};
		return _this;
	}
]);
