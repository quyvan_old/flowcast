'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Application = mongoose.model('Application'),
	_ = require('lodash');

/**
 * Create a Application
 */
exports.create = function(req, res) {
	var application = new Application(req.body);
	application.user = req.user; //how do you possibly get the user from the request ? I guess its a field in application yo
	//THE USER IS ALWAYS PART OF THE REQUEST ? 

	application.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(application);
		}
	});
};

/**
 * Show the current Application
 */
exports.read = function(req, res) {
	res.jsonp(req.application);
};

/**
 * Update a Application
 */
exports.update = function(req, res) {
	var application = req.application ;

	application = _.extend(application , req.body);

	application.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(application);
		}
	});
};

/**
 * Delete an Application
 */
exports.delete = function(req, res) {
	var application = req.application ;

	application.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(application);
		}
	});
};

/**
 * List of Applications
 */
exports.list = function(req, res) { 


	var casting_id = req.param('castingIdApp');
	var userId = req.param('userId');
	var voteStatusFlag = req.param('voteStatus');




	if (req.param('qstats')) {
		Application.find().exec(function (err,applications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.jsonp(applications);
			}
		});
	}
	else if (casting_id) {
		if (voteStatusFlag === undefined || voteStatusFlag === '') {
			Application.find({casting_applied_for : casting_id}).
			populate('casting_applied_for').populate('user').populate('employee').exec(function(err, applications) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.jsonp(applications);
				}
			});

		}
		else {
			Application.find({casting_applied_for : casting_id, voteStatus : voteStatusFlag}).
			populate('casting_applied_for').populate('user').populate('employee').exec(function(err, applications) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.jsonp(applications);
				}
			});
		}
	}
	else if (userId) { //get all the applications for a specific user (doesnt' have to be user making the call)
		Application.find({user : userId}).populate('casting_applied_for').populate('user').populate('employee').exec(function(err, applications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(applications);
			}
		});

	}

	else { //default behaviour - get all the applications for the user making the call 
		Application.find({user : req.user._id}).populate('casting_applied_for').populate('user').populate('employee').exec(function(err, applications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(applications);
			}
		});
	}
};


exports.listByCastingID = function(req,res){
	//console.log('in this new list by casting id method');

	res.jsonp(req.application);

};


/**
 * Application middleware ---> puts the application object into the request ?
 */
exports.applicationByID = function(req, res, next, id) { 
	//console.log('called the applicationByID middleware yo');
	Application.findById(id).populate('user').populate('casting_applied_for').populate('employer').populate('employee').exec(function(err, application) { //dont populate by display name --> messes shit up ?
		if (err) return next(err);
		if (! application) return next(new Error('Failed to load Application ' + id));
		req.application = application ;
		next();
	});
};



/* exports.applicationsByCastingID = function(req, res, next, id) { 
	//console.log('called the application castingId middleware yo');
	Application.find({casting_applied_for : id}).populate('casting_applied_for','user').exec(function(err, applications) {
		if (err) return next(err);
		if (! application) return next(new Error('Failed to load Application ' + id));
		//console.log(applications);
		//req.application = application ;
		next();
	});
	next();
}; */

/*
* Public sign in authorization middleware
*/

exports.haspublicAuthorization = function (req,res,next){

	if (!req.user) { //make sure the user actually exists? Should probably check user._id or something b/c user is always there ?
		return res.status(403).send('Please create an account or sign in to continue');
	}
	next();

};


/**
 * Application authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.application.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
