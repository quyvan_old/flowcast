'use strict';

//Setting up route
angular.module('castings').config(['$stateProvider',
	function($stateProvider) {
		// Castings state routing
		$stateProvider.
		state('listCastings', {
			url: '/castings',
			templateUrl: 'modules/castings/views/list-castings.client.view.html'
		}).
		state('listMyCastingPosts', {
			url: '/castings/mycastingposts',
			templateUrl: 'modules/castings/views/list-my-casting-posts.client.view.html'
		}).
		state('listMyAppliedCastings', {
			url: '/castings/myappliedcastings',
			templateUrl: 'modules/castings/views/list-my-applied-castings.client.view.html'
		}).
		state('createCasting', {
			url: '/castings/create',
			templateUrl: 'modules/castings/views/create-casting.client.view.html'
		}).
		state('viewCasting', {
			url: '/castings/:castingId',
			templateUrl: 'modules/castings/views/view-casting.client.view.html'
		}).
		state('editCasting', {
			url: '/castings/:castingId/edit',
			templateUrl: 'modules/castings/views/edit-casting.client.view.html'
		});
	}
]);
