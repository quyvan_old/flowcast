//No longer implemeneted, keeping as reference
'use strict';

angular.module('reviews').factory('Viewreviewfields', [
	function() {
		// Viewappfields service logic
		// ...
		var  create_fields =  ['role','score','description','completed'];
		var editable_fields = ['score','description'];

		var getCreateFields = function(){
			return create_fields; 
		};

		var getEditFields = function(){
			return editable_fields; 
		};


		// Public API - can have specific methods and what they return here
		return {

			createFields : function() {return getCreateFields(); }, //list of methodName : what it returns ---> usually a function 
			editFields : function() {return getEditFields(); }

};

}]);
