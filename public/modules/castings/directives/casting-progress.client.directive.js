//deprecated and no longer working... leave here for future reference - quy
'use strict';

angular.module('castings').directive('getApplicationCastingProgressField', 
	function() {
		var directive = {};

		directive.restrict ='E'; //values can be a combination of 'E','A','C' ...ex 'EA' or 'EAC'
		directive.scope = {application: '=info'}; //isolated scope... variables only used for this instance.. which allows ng-repeat to workwithout replacing  $scope(parent/controller/global...whatever it is) values
		directive.templateUrl = 'modules/applications/views/templates/application-list.client.template.html';
		return directive;

});


