'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Review = mongoose.model('Review'),
	_ = require('lodash');

/**
 * Create a Review
 */
exports.create = function(req, res) {
	var review = new Review(req.body);
	//review.user = req.user;   -------------> REMOVED ONLY BECAUSE IN REVIEWS WE EXPLICITLY SET THE USER 

	review.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(review);
		}
	});
};

/**
 * Show the current Review
 */
exports.read = function(req, res) {
	res.jsonp(req.review);
};

/**
 * Update a Review
 */
exports.update = function(req, res) {
	var review = req.review ;

	review = _.extend(review , req.body);

	review.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(review);
		}
	});
};

/**
 * Delete an Review
 */
exports.delete = function(req, res) {
	var review = req.review ;

	review.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(review);
		}
	});
};

/**
 * List of Reviews
 */
exports.list = function(req, res) { 

	//expect to have userId, isCompleted, and isCreateUser
	//should probably setup defaults
	//but for now... userId defaults to req.user

	//console.log(req.param('userId'));
	//can't we just do req.user anyway ? So you would only get the reviews corresponding to a specific user...
	//how does req.user even get populated anyway??

	var userId; 

	//see if this parameter was provided, if not just use the default user id ?
	if (req.param('userId')){
		userId = req.param('userId');
	}

	else{
		userId = req.user._id; 
	}


//	var userId = req.user._id; 
//	//console.log(req.user);

	var isCompleted = req.param('isCompleted'); //this is bad b/c we dont check if this is actually defined, but it always should be
	//todo: fix this code, make it cleaner , give this a default behaviour 
	//console.log(req.param('isCreateUser'));


	if (req.param('isCreateUser')==='true'){ //search for reviews CREATED BY the current user 
	//	//console.log('in the list function bro');
	//	var userId = req.param('userId');

		//check if you want to see completed reviews or not ? 
		//console.log('searching for all reviews CREATED BY this user....');

		Review.find({userCreated : userId, completed: isCompleted}).sort('-created').populate('userCreated').populate('user').populate('userFor').exec(function(err, reviews) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.jsonp(reviews);
			}
		});

	}

	else { //search for reviews that are ABOUT the current user

	//console.log('searching for all reviews FOR this user....');
	Review.find({userFor : userId, completed: isCompleted}).sort('-created').populate('userCreated').populate('user').populate('userFor').exec(function(err, reviews) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(reviews);
		}
	});

}

};

/**
 * Review middleware
 */
exports.reviewByID = function(req, res, next, id) { 
	Review.findById(id).populate('userCreated').populate('user').populate('userFor').exec(function(err, review) { //need to use populate multiple times
		if (err) return next(err);
		if (! review) return next(new Error('Failed to load Review ' + id));
		req.review = review ;
		next();
	});
};

/**
 * Review authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.review.userCreated.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
