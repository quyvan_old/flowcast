'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', '$location', 'Notifications','NotificationUtil', 'UserCheck',
	function($scope,Authentication, Menus, $location, Notifications, NotificationUtil, UserCheck) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');
		$scope.num_notifications = 0; 
		$scope.userCheck = UserCheck;


		//console.log($scope.menu.items[2]);
	//	//console.log($scope.toggleCollapsibleMenu());
		

		/*no longer need this, but will keep as reference
		$scope.$watch('num_notifications',function(newVal,oldVal){
			//update the menu and stuff here

			if (newVal !== 0){
						$scope.notification_string = 'Notifications' + '(' + newVal + ')';
					}
					else{
						$scope.notification_string = 'Notifications';
					}
					
				});
		*/
		
		//update notification bar based on how many things are unseen
		//todo: make this a function in notification util, so that you can immediately update it when you view a notificaiton?
		//check that the user is logged in before you do this!
		var notification_update_function = function() {
			//only update notifications if user is authentictaed/signed in
			if ($scope.authentication.user._id){
				//should only query for objects that have seen:false would probably speed things up
				var notifications = Notifications.query({seen : false}, function(response){ 				
					var notif_counter = response.length; 
					$scope.num_notifications =  notif_counter; 
				});	   	 
			}
		};
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};
		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
		if (Authentication.user) {
			var myVar = window.setInterval(notification_update_function,5000);
			notification_update_function(); //call immediately upon page loading 
		}
}]);
