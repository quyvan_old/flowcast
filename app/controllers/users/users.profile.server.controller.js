'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller.js'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User');


/**
 * Update user details without affecting hte url ---> shouold  probably change this to just have a flag in req.
 */
exports.updateNoUrl = function(req, res) {
	// Init Variables
	var user = req.user;
	var message = null;

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		user.displayName = user.firstName + ' ' + user.lastName;
		User.count({displayName: user.displayName}, function (err, result) {
			var userDisplayNameCount = result;	


			user.save(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					req.login(user, function(err) {
						if (err) {
							res.status(400).send(err);
						} else {
							res.json(user);
						}
					});
				}
			});
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};

/**
 * Update user details
 */
exports.update = function(req, res) {
	// Init Variables
	var user = req.user;
	var message = null;
	var updateCountFlag = req.param('noUrlUpdate');

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	//console.log(req.param('fag'));

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		user.displayName = user.firstName + ' ' + user.lastName;
		User.count({displayName: user.displayName}, function (err, result) {
			var userDisplayNameCount = result;	
			if (!updateCountFlag){ //if this flag doesn't exist then go ahead and update the user url
				//if IT does exist, it means we don't watn the current user to be included in the count
				//this was always causign the coutn to be changed from 1 --> 2 (at the end of the user url)
				//gawd quy u fucked up
				user.userUrl = user.firstName + '_' + user.lastName + '_'  + (userDisplayNameCount + 1).toString();
			}
			


			user.save(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					req.login(user, function(err) {
						if (err) {
							res.status(400).send(err);
						} else {
							res.json(user);
						}
					});
				}
			});
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};


//used to view a profile
exports.read = function(req, res) {
	res.jsonp(req.viewUser || null);
};

/**
 * Send User
 */
exports.me = function(req, res) {
	res.json(req.user || null);
};

//List users
exports.list = function(req, res) { 
	User.find().sort('-created').exec(function(err, users) { 
		if (err) {						
			return res.status(400).send({	
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(users);
		}
	});


};


exports.userByUserUrl = function(req, res, next, userUrl) {
	User.findOne({
		userUrl: userUrl
	}).exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next(new Error('Failed to load User ' + userUrl));
		req.viewUser = user; // this is used specifically to view user
		next();
	});
};

