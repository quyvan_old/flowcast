'use strict';

// Reviews controller
angular.module('reviews').controller('ReviewsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Reviews', 'UserObject', 'UserCheck',
	function($scope, $stateParams, $location, Authentication, Reviews, UserObject, UserCheck) {
		$scope.authentication = Authentication;
		$scope.userCheck = UserCheck;
		$scope.descriptionMaxLength=1000;

		// Create new Review
		$scope.create = function() {
			// Create new Review object
			var review = new Reviews ({
				title: this.title
			});

			// Redirect after save
			review.$save(function(response) {
				$location.path('reviews/' + response._id);

				// Clear form fields
				$scope.title = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Review
		$scope.remove = function(review) {
			if ( review ) { 
				review.$remove();

				for (var i in $scope.reviews) {
					if ($scope.reviews [i] === review) {
						$scope.reviews.splice(i, 1);
					}
				}
			} else {
				$scope.review.$remove(function() {
					$location.path('reviews');
				});
			}
		};

		// Update existing Review
		$scope.update = function() {
			var review = $scope.review;

			review.$update(function() {
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		$scope.updateAndRedirect = function() {
			var review = $scope.review;

			review.$update(function() {
				alert ('Review not yet finalized and submitted. Please hit the submit button on the next page.');
				$location.path('reviews/' + review._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		//Submit a review and update the relevant stuff
		$scope.accept = function(){
			var curr_review = $scope.review;
			curr_review.completed = true;
			curr_review.$update(function(){
				$location.path('/castings/' + curr_review.casting);
			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			}); 

		};


		// Find a list of Reviews
		$scope.find = function() {
			$scope.reviews = Reviews.query();
		};

		// Find existing Review
		$scope.findOne = function() {
			if ($stateParams.castingId){
				$scope.review = $scope.casting.review;
			
			}
			else if ($stateParams.reviewId){
				$scope.review = Reviews.get({ 
					
					reviewId: $stateParams.reviewId
				});
				
			}
		};
		// Find existing Review
		$scope.redirectIfNotAuthorizedFindOne = function() {
			if ($stateParams.castingId){
				$scope.review = $scope.casting.review;
			
			}
			else if ($stateParams.reviewId){
				$scope.review = Reviews.get({ 
					reviewId: $stateParams.reviewId
				}, function (response) {
					console.log(response.user);
					UserCheck.redirectIfNotAuthorizedUser($scope.authentication.user, response.user);
				
				});
				
			}
		};
		// Find existing Review
		$scope.findOneAndRedirectIfEmpty = function() {
			$scope.review = Reviews.get({ 
				
				reviewId: $stateParams.reviewId
			}, function (review) {
				if (review.score === '' || review.score === undefined) {
					$location.path('/reviews/' + review._id + '/edit'); //redirect back to the home page ? 

				}
			
			});
		};

		// Find all reviews for a specific user (the current user in the default case?)
		$scope.findByUser = function(complete_option,byCreateUser) {
			//complete option ---> whether you are searching for completed or pending reviews
			// byCreateUser ---> whether you are looking for cases where you CREATED the review 
			//(if not, the default is obviously that you are looking for reviews where the review is ABOUT you 
			//(i.e your username is in the userFor field))
			var curr_user_id = $scope.authentication.user._id;
			$scope.reviews = Reviews.query({
				userId : curr_user_id,
				isCompleted : complete_option,
				isCreateUser : byCreateUser
			}); 
		};

		$scope.findByProfileUser  = function(complete_option,byCreateUser) {
			//complete option ---> whether you are searching for completed or pending reviews
			// byCreateUser ---> whether you are looking for cases where you CREATED the review 
			//(if not, the default is obviously that you are looking for reviews where the review is ABOUT you 
			//(i.e your username is in the userFor field))

			//kind of a hacky way to get the user Id ---> by querying it by username and then getting the id field
			//all because quy uses the username instead of id in the profile page qq
			var profile_user = UserObject.get({
				userUrl: $stateParams.userUrl
				}, function(response){

				$scope.reviews = Reviews.query({
					userId : response._id,
					isCompleted : complete_option,
					isCreateUser : byCreateUser
					}, function(){
						var tot_score = 0; 

						if ($scope.reviews.length > 0){

						for (var i = 0; i < $scope.reviews.length; i++){
							tot_score = tot_score + $scope.reviews[i].score; 
						}
						$scope.avg_score = tot_score/($scope.reviews.length);
					}
					else{
						$scope.avg_score = 'No reviews have been posted about this user yet!';
					}

				}); 
			});
		};
}]);
