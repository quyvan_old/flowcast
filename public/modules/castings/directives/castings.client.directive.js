'use strict';

angular.module('castings').directive('listcastings', 
	function() {
		return {
			templateUrl: 'modules/castings/views/list-castings-limited.client.view.html'
		};
});


