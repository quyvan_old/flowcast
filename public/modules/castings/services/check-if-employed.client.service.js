'use strict';

//Castings service used to communicate Castings REST endpoints
angular.module('castings').factory('CheckIfEmployed', [
	function() {
		var _this = this;

		_this.checkIfEmployed = function (userOneId, userTwoId){
			if (userOneId === userTwoId) {
				return true;
			}
			return false;
		};
		return _this;
	}
]);
