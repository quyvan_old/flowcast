'use strict';
/*
This directive is used to display a field from an object

Example useage:
		<display-field inject-type="text" inject-model="casting" inject-field="gender">
		</display-field>

You must use:
	- the type attribute which tells the directive which type of data to expect
	- the model attribute which passes the object to be read from
	- the field attribute which passes which field to read
*/

angular.module('core').directive('displayField', //use 'display-field' in html to call
	function() {
		var directive = {};

		directive.scope = { //use hyphenated form... ie 'inject-Href in html to call
			type: '@injectType',
			model: '=injectModel',
			field: '@injectField'

		};
		directive.link = function (scope, element, attrs){
		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/core/views/display-field.client.view.html';
		return directive;
});
