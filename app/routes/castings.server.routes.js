'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var castings = require('../../app/controllers/castings.server.controller');

	// Castings Routes
	app.route('/castings')
		.get(castings.list)
		.post(users.requiresLogin, castings.create);

	app.route('/castings/:castingId')
		.get(castings.read)
		.put(users.requiresLogin, castings.haspublicAuthorization, castings.update)
		.delete(users.requiresLogin, castings.hasAuthorization, castings.delete);

	// Finish by binding the Casting middleware
	app.param('castingId', castings.castingByID); //this comes from the URL?? ---> pretty much saying use the casting id parameter in the URL in this functioN?
	//see http://webapplog.com/intro-to-express-js-parameters-error-handling-and-other-middleware/
	//middleware executes BEFORE other functions??

	//param automatically injects middleware into our app ? whenever the castingId is passed  as a URL arguement?
};
