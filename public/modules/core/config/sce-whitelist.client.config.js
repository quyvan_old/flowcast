'use strict';

// Setting up route
angular.module('core').config(['$sceDelegateProvider',
	function($sceDelegateProvider) {
		$sceDelegateProvider.resourceUrlWhitelist([
			'self',
			'*://www.youtube.com/**'
		]);

	}
]);
