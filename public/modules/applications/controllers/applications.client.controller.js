'use strict';

// Applications controller
angular.module('applications').controller('ApplicationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Applications','Castings', 'Viewappfields','Users','Reviews', 'UserCheck','NotificationUtil','Notifications', 'CheckIfEmployed', 'CreateNotification', 'GlobalSettings', 'Pagination',
	function($scope, $stateParams, $location, Authentication, Applications,Castings, Viewappfields,Users,Reviews, UserCheck,NotificationUtil,Notifications, CheckIfEmployed, CreateNotification, GlobalSettings, Pagination) {
		$scope.authentication = Authentication;
		$scope.viewappfields = Viewappfields.createFields();
		$scope.pagination=Pagination;
		$scope.user = Authentication.user; 
		$scope.messageMaxLength=1000;
		$scope.newApplication={};
		$scope.userCheck = UserCheck;
		$scope.globalSettings = GlobalSettings;


		$scope.createNotification = function (name, user, createdBy, message) {
			CreateNotification.createNotification(name, user, createdBy, message);
		};

		$scope.checkIfEmployed = function (application){
			var employee = application.casting_applied_for.employee;
			if (employee === '' || employee === undefined || employee === null) {
				return false;
			}
			return CheckIfEmployed.checkIfEmployed($scope.user._id, employee);
		};
		
		// Create new Application/Application
		$scope.create = function() {

			// Create new Application object
			//use a service to create a notfication

			var application = new Applications ({
				name: $scope.authentication.user.displayName,
				user: $scope.authentication.user._id, //store a reference to 	user ----> this is automatically set anyway
				contactInfo: $scope.newApplication.contactInfo,
				message: $scope.newApplication.message,
				voteStatus: 'none',
				casting_applied_for: $stateParams.castingId //store a reference to the casting this application is about 
			  
			});
			$scope.newApplication={};

			//you need to have a callback function or something...callback function is used after the query is complete?
			//you can't access the JSON object outside of a callback function?
			//because it executes in a linear manner? You might not even have the object yet....thats what the $promise is for
			var mycasting = Castings.get({     //use the casting service to query for the specific casting you are creating an application for
				castingId: $stateParams.castingId
			}, function(){
				application.employer = mycasting.user._id; //store a reference to the employer of the casting (used in authentication)
				var profile_url = mycasting.user.userUrl;
				application.$save(function(response) { //save the application

					mycasting.applications.push(response._id); //save a reference to the application in the casting object

					mycasting.$update(function() {
						$location.path('applications/' + application._id);
						}, function(errorResponse) {
						$scope.error = errorResponse.data.message;
						});


				 	//CREATE THE NOTIFICATION UPON CREATING AN APPLICATION
					var user_link = NotificationUtil.createHTMLStringUser('user',$scope.authentication.user.userUrl);  //pass in user and message 
					var application_link = NotificationUtil.createHTMLStringApplication('Click here',response._id);
					var message = 'A ' + user_link +  ' applied to your casting. ' + application_link + ' to see the application';

					$scope.createNotification('Someone applied to your casting!',mycasting.user._id,'System',message);

				}); 
			}); 

		};

		//Save andHide feature when viewing application---------------------
		$scope.setApplications = function (list) {
			$scope.applications=list;
		};
		var setVoteStatusAndSave = function (voteStatusInput) {
			var application = $scope.application; 
			application.voteStatus = voteStatusInput;
			$scope.update(function () {
			});

		};

		$scope.hide = function(application){
			setVoteStatusAndSave('downvoted');
			$scope.pagination.removeItemFromList(application,$scope.applications);
		};
		//function to accept an application
		$scope.accept = function(application){
			setVoteStatusAndSave('upvoted');
			$scope.pagination.removeItemFromList(application,$scope.applications);
		};
		//---------------------------------------------------

		//should this be in a service ? 
		$scope.createPendingReview = function(userFor,userCreated,casting,role_name){
			//userCreated ---> user who is creating the review
			//userFor ---> user who the review is FOR
			//role ---> role o fthe user you are reviewing (employee/employer)
			//casting ---> casting this review is about 

			//console.log('automatically creating review...');

			var review_title = 'Review for user '.concat(String(userFor.username));


			//inject review service and use it here to create a new review ---> then they can see it in pending reviews?
			//werid bug where userFor and user show up as the same thing in the //console.log() debugger in chrome, but
			//they actually have different values if you print it out ? 
			var review = new Reviews ({
				title: review_title,   //name should be like Employee review for casting id # XYZ or Employer Review for casting XYZ
				userFor : userFor._id,
				role : role_name, 
				casting: casting._id,
				userCreated : userCreated._id,
				completed: false 
			});


			

			// Redirect after save
			review.$save(function(response) {
				$location.path('reviews/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			
		};


		// Remove existing Application
		$scope.remove = function(application) {
			var confirmed = confirm('Are you absolutely sure you want to delete?');   
			if (confirmed) {
				if ( application ) { 
					application.$remove();

					for (var i in $scope.applications) {
						if ($scope.applications [i] === application) {
							$scope.applications.splice(i, 1);
						}
					}
				} else {
					$scope.application.$remove(function() {
						$location.path('applications');
					});
				}
			}
		};

		// Update existing Application
		$scope.update = function() {
			var application = $scope.application;

			application.$update(function() {
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Update existing Application
		$scope.updateAndRedirect = function() {
			var application = $scope.application;

			application.$update(function() {
				$location.path('applications/' + application._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of ALL Applications for a given user ? 
		$scope.find = function() {
			$scope.applications = Applications.query( {userId : $scope.authentication.user._id });
		};

		// Find all applications for a specific casting?
		// optional variable userId is used in process of checking whether a casting has been applied to by current user
		$scope.findSpecific = function(userId, voteStatusInput) {
			$scope.applications = Applications.query({
				castingIdApp : $stateParams.castingId,
				voteStatus: voteStatusInput
			}, function(response){
				if (userId !== '' || userId !== undefined){
					checkIfApplied(userId, response);
				}
			});
			
		};
		$scope.redirectIfApplied = function () {
			Applications.query({
				castingIdApp : $stateParams.castingId,
			}, function(response){
				checkIfApplied(Authentication.user._id, response);
				if ($scope.alreadyapplied){
					alert('You have already applied');
					$location.path('/castings/' + $stateParams.castingId);
				}

			});

		};

		var checkIfApplied = function (userId, response) {
			if (userId !== '' || userId !== undefined){
				var testId;
				$scope.alreadyapplied = false;
				for (var index in response)
				{
					if (index % 1 !== 0){ // there are other results such as "promise" that needs to be ignored
						break;
					}
					$scope.application = response[index];
					testId = $scope.application.user._id;
					if (testId === userId){
						$scope.alreadyapplied = true;
						break;
					}
				}
			}
		};

		// Find existing Application
		$scope.findOne = function() {
			$scope.application = Applications.get({ 
				applicationId: $stateParams.applicationId

			}, function () {
			});
		
		};
		$scope.redirectIfNotEitherFindOne = function() {
			$scope.application = Applications.get({ 
				applicationId: $stateParams.applicationId

			}, function () {
				$scope.userCheck.redirectIfFirstIsNotEither($scope.authentication.user, $scope.application.user, $scope.application.employer);
				
				//now check if it has been viewed before? 
				console.log('checking if application has been viewed before...');
				console.log($scope.application);

				//need to get the user who created the job of the initial application?
				//console.log($scope.application.employer.displayName); //casting id of the application you applied for



				if (!$scope.application.viewed && $scope.user._id === $scope.application.employer._id){
					console.log('the employer is viewing your application...sending out a viewed notification');
					//link back to the job so it can be liek..."Someone viewed your application for casting <link>"
					var msg = NotificationUtil.createHTMLStringCasting('click here',$scope.application.casting_applied_for._id);
					$scope.createNotification('Your application has been viewed!',$scope.application.user._id,'System', msg + ' to see the casting you applied to');
					console.log('updating application...');
					$scope.application.viewed = true;
					var application = $scope.application; 
					$scope.update();

				} 


			});
		
		};
		// Find existing Application
		$scope.ifNotHiringOrAuthorizedRedirectFindOne = function() {
			$scope.application = Applications.get({ 
				applicationId: $stateParams.applicationId

			}, function (response) {
				var casting = response.casting_applied_for;
				if (casting.progress!=='hiring') {
					alert ('The casting is no longer in the process of hiring');
					$location.path('/castings/' + casting._id);
				}
				UserCheck.redirectIfNotAuthorizedUser($scope.authentication.user, response.user);
			});
		
		};
	}
]);
