'use strict';
/* 
This directive is used to display a list with pagination navigation

Example useage:
<div data-ng-controller="CastingsController" data-ng-init="find()"> //the find function sets castings=[{...},{...},...]
<display-list inject-list="castings" module-name="casting"></display-list>

You must use:
	- the list attribute... which is a collection of <module> objects
	- the moduleName attribute (singular; without 's') 
Other optional attributes are: 
	-viewType ... which determines how the list will be displayed.. for exmaple 'grid' ... default is row
	-customTemplate... which passes the template to use in  preview-<module-name> which is specific the the preview-<module-name> html page
	-customFunction... which passes a function to use in  preview-<module-name> which is specific to the preview-<module-name> html page

**when using optional attributes, make sure it's implemented in the display-list html page

If you are including a new module, modify the display-list html page located at "../views/display-list.client.view.html"
You must also create 
	1)a "preview-<module-name>" directive to call it in the display-list html page
	2)an html page for the preview-<module-name> to display what a single item from the list will display
*/

angular.module('core').directive('displayList', ['Pagination', //use 'display-list' in html to call
	function(Pagination) {
		var directive = {};

		directive.scope = { //use hyphenated form... 'module-name' instead of 'moduleName' in html
			list: '=injectList', // the collection... must be initiated before, so that pagination can be initiated in the display-list html page
			moduleName: '@injectModuleName', //ie casting, application, review
			viewType: '@injectViewType',//used to pass template formate in the display-list html page... ie 'row', 'grid'
			customTemplate: '@injectCustomTemplate',//used to pass the template format to use in the preview-<module-name> html page
			customFunction: '&injectCustomFunction'//used if a special function is needed to be passed to direcitive
		};
		directive.link = function ($scope, element, attrs){
			$scope.pagination = Pagination;

		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/core/views/display-list.client.view.html';
		return directive;
}]);
