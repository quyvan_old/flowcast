'use strict';
angular.module('core').factory('Pagination', [
	function(){
		var _this=this;

		_this.setTotalItems = function (number) {
			_this.totalItems = number;
		};

		_this.start = function (totalItems){
			_this.setTotalItems(totalItems);
			_this.currentPage=1;
			_this.numberOfPages = 10;
			_this.itemsPerPage = 12;
			_this.maxSize = 5;
		};
		_this.removeItemFromList = function (item, items) {
			for (var i in items) {
				if (items[i].id === item.id) {
					items.splice(i, 1);
					_this.totalItems--;
					return true;
				}
			}
			return false;

		};

		return _this;
	}

]);
