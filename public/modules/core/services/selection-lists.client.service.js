'use strict';
angular.module('core').factory('SelectionLists', [
	function(){
		var _this=this;
		_this.castingCharacteristics = ['skills','languages','gender','union','portrayableEthnicity', 'portrayableAgeMin', 'portrayableAgeMax', 'heightMin', 'heightMax'];
		_this.userCharacteristics = ['skills','languages','gender','union','portrayableEthnicity', 'portrayableAgeMin', 'portrayableAgeMax', 'height'];

		_this.skills=['Dancing','Singing', 'Stunts', ];
		_this.languages=['English','French'];
		_this.genders=['Male','Female'];
		_this.castingUnion=['Union','Non-Union', 'Union and Non-Union'];
		_this.userUnion=['Non-Union', 'ACTRA', 'Other Union'];
		_this.portrayableEthnicity=['White/Caucasian', 'Asian', 'Middle Eastern', 'African', 'Native American'];
		return _this;
	}

]);
