'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Casting = mongoose.model('Casting'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, casting;

/**
 * Casting routes tests
 */
describe('Casting CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Casting
		user.save(function() {
			casting = {
				name: 'Casting Name'
			};

			done();
		});
	});

	it('should be able to save Casting instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Casting
				agent.post('/castings')
					.send(casting)
					.expect(200)
					.end(function(castingSaveErr, castingSaveRes) {
						// Handle Casting save error
						if (castingSaveErr) done(castingSaveErr);

						// Get a list of Castings
						agent.get('/castings')
							.end(function(castingsGetErr, castingsGetRes) {
								// Handle Casting save error
								if (castingsGetErr) done(castingsGetErr);

								// Get Castings list
								var castings = castingsGetRes.body;

								// Set assertions
								(castings[0].user._id).should.equal(userId);
								(castings[0].name).should.match('Casting Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Casting instance if not logged in', function(done) {
		agent.post('/castings')
			.send(casting)
			.expect(401)
			.end(function(castingSaveErr, castingSaveRes) {
				// Call the assertion callback
				done(castingSaveErr);
			});
	});

	it('should not be able to save Casting instance if no name is provided', function(done) {
		// Invalidate name field
		casting.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Casting
				agent.post('/castings')
					.send(casting)
					.expect(400)
					.end(function(castingSaveErr, castingSaveRes) {
						// Set message assertion
						(castingSaveRes.body.message).should.match('Please fill Casting name');
						
						// Handle Casting save error
						done(castingSaveErr);
					});
			});
	});

	it('should be able to update Casting instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Casting
				agent.post('/castings')
					.send(casting)
					.expect(200)
					.end(function(castingSaveErr, castingSaveRes) {
						// Handle Casting save error
						if (castingSaveErr) done(castingSaveErr);

						// Update Casting name
						casting.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Casting
						agent.put('/castings/' + castingSaveRes.body._id)
							.send(casting)
							.expect(200)
							.end(function(castingUpdateErr, castingUpdateRes) {
								// Handle Casting update error
								if (castingUpdateErr) done(castingUpdateErr);

								// Set assertions
								(castingUpdateRes.body._id).should.equal(castingSaveRes.body._id);
								(castingUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Castings if not signed in', function(done) {
		// Create new Casting model instance
		var castingObj = new Casting(casting);

		// Save the Casting
		castingObj.save(function() {
			// Request Castings
			request(app).get('/castings')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Casting if not signed in', function(done) {
		// Create new Casting model instance
		var castingObj = new Casting(casting);

		// Save the Casting
		castingObj.save(function() {
			request(app).get('/castings/' + castingObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', casting.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Casting instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Casting
				agent.post('/castings')
					.send(casting)
					.expect(200)
					.end(function(castingSaveErr, castingSaveRes) {
						// Handle Casting save error
						if (castingSaveErr) done(castingSaveErr);

						// Delete existing Casting
						agent.delete('/castings/' + castingSaveRes.body._id)
							.send(casting)
							.expect(200)
							.end(function(castingDeleteErr, castingDeleteRes) {
								// Handle Casting error error
								if (castingDeleteErr) done(castingDeleteErr);

								// Set assertions
								(castingDeleteRes.body._id).should.equal(castingSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Casting instance if not signed in', function(done) {
		// Set Casting user 
		casting.user = user;

		// Create new Casting model instance
		var castingObj = new Casting(casting);

		// Save the Casting
		castingObj.save(function() {
			// Try deleting Casting
			request(app).delete('/castings/' + castingObj._id)
			.expect(401)
			.end(function(castingDeleteErr, castingDeleteRes) {
				// Set message assertion
				(castingDeleteRes.body.message).should.match('User is not logged in');

				// Handle Casting error error
				done(castingDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Casting.remove().exec();
		done();
	});
});