'use strict';

angular.module('core').controller('QstatsController', ['$scope','Users', 'Castings','Applications',
	function($scope, Users, Castings, Applications) {
		$scope.initNumberOfLoginVars = function() {
			Users.query(function(response){ 
				var currentUser;
				var totalLogins = 0;
				var totalUsers = 0;
				var totalPics = 0;
				for (var usersIndex in response) {
					currentUser = response[usersIndex];
					if (currentUser.email) { //if user exist via check if they have email field
						totalUsers ++;
					}
					else {
						continue;//go to next
					}

					//Logins
					if (currentUser.totalLogins !== undefined) {
						totalLogins += currentUser.totalLogins;
					}

					//Profile Pics
					if (currentUser.profilePicUrl) {
						totalPics ++;
					}
				}
				$scope.totalLogins = totalLogins;
				$scope.totalUsers = totalUsers;
				$scope.totalPics = totalPics;
			});

			//Keep as query, in case we need to gather other details
			Castings.query(function (response ) {
				var currentCasting;
				var totalCastings = 0;
				for (var castingsIndex in response) {
					currentCasting = response[castingsIndex];
					if (currentCasting.progress) {//if casting exists via check if they have progress field
						totalCastings++;
					}
					else {
						continue;
					}
				}
				$scope.totalCastings = totalCastings;
			});
			Applications.query({qstats:true},function (response) {
				var currentApplication;
				var totalApplications = 0;
				for (var applicationsIndex in response) {
					currentApplication = response[applicationsIndex];
					if (currentApplication.name) {
						totalApplications++;
					}
					else {
						continue;
					}
				}
				$scope.totalApplications = totalApplications;
			});
		};
	}
]);
