'use strict';

angular.module('users').factory('S3Conn', ['Menus', '$rootScope',
     	// Viewappfields service logic
		

		function(Menus, $rootScope) {

		var upload_crap = 0 ;

		var creds = {
  				bucket: 'gigapp-bucket',  //can create a dynamic directory based on user url name
  				//this is not really the same as creating a new bucket...just a new folder ? 
  				access_key: 'AKIAJWXHN55D43BXHRPA',
  				secret_key: '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'
  		};
		var aws_region = 'us-west-2';
		var aws_url_prefix =  'https://s3-us-west-2.amazonaws.com/gigapp-bucket/'; //starting url for aws

		//upload Object to S3 
		//file: file to be uploaded
		//path: directory (not including the bucket part) that specifies the folder where the file is uploaded to (JUST THE FOLDER)
		//path doesn't need an ending '/' it is added automatically
		//cb --> callback function to execute since this is async
		var  s3UploadObject = function(file,path,cb,cb_update){
		   //update global AWS variable
		   AWS.config.update({ accessKeyId: creds.access_key, secretAccessKey: creds.secret_key });
		   console.log(creds.access_key);
		   AWS.config.region =  aws_region;		
		   var bucket = new AWS.S3({ params: { Bucket: creds.bucket } });
		   if (file){
		   	//Key is like the directory ?
		   	 var params = { Key: path+'/'+file.name, ContentType: file.type, Body: file, ServerSideEncryption: 'AES256' };
		   	 console.log(file.size); //do some checking on the file size and extension here


			  bucket.putObject(params, function(err, data) {
			  var public_url;
			  public_url = 'https://s3-us-west-2.amazonaws.com/' + creds.bucket + '/' + path + '/' + file.name.replace(' ','+');
			  upload_crap = 0; //set the progress back to zero 
		      cb(err,data,public_url); //return through callback function 
		    }).on('httpUploadProgress',function(progress) {

		    	cb_update(); //update scope (which is watching the upload_crap variable pretty much)
		    	upload_crap = Math.round(progress.loaded / progress.total * 100); //update progress
         	 	console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
        });
  }

		};


		//filename: name of file to be removed
		//path: location of file (after the bucket...don't include the filename) ---> don't include a trailing '/'
		//cb ---> callback function to execute since this is async
		var s3RemoveObject = function(filename,path,cb){ //link to user profile 
		   //update global AWS variable
		   AWS.config.update({ accessKeyId: creds.access_key, secretAccessKey: creds.secret_key });
		   AWS.config.region = aws_region;		

		   var s3 = new AWS.S3({ params: { Bucket: creds.bucket } });
		   var params = {Key: path+'/'+filename, Bucket: creds.bucket };
		   console.log(path+'/'+filename);

		   s3.deleteObject(params, function(err, data) {
  				cb(err,data);
		
		});

			
		};

		//path --> folder path/directory to get all the files in (after the bucket part..)
		//if left blank, just gets all the files on the root of the bucket in s3
		//cb ---> callback function to execute since this is async
		var s3ListObjects = function(path,cb){
		   //update global AWS variable
		   AWS.config.update({ accessKeyId: creds.access_key, secretAccessKey: creds.secret_key });
		   AWS.config.region = aws_region;		
		   var s3 = new AWS.S3({ params: { Bucket: creds.bucket } });
		   var params = { Bucket: creds.bucket, Prefix: path};


		   s3.listObjects(params, function(err, data) {
		   		var temp = [];
  				if (err) {
  					console.log(err, err.stack); // an error occurred
  				}
  				else     
  				{
  					//console.log(data);           // successful response
  					var i;
  					
  					for (i=0; i < data.Contents.length; i++){
  						//console.log(data.Contents[i].Key);
  						temp.push(data.Contents[i].Key);
  					}

  					//do cb stuff here
  				}

  				cb(err,data,temp); //return stuff through callback
		});
		};


		// Public API - can have specific methods and what they return here
		return {
			aws_prefix : function(){return aws_url_prefix;},
			upload_progress : function() {return upload_crap;}, //returns upload progross
			listObjects : function(path,cb) {return s3ListObjects(path,cb); }, //list of methodName : what it returns ---> usually a function 
			putObject : function(file,path,cb,cb_update) {return s3UploadObject(file,path,cb,cb_update); },
			//cb_update is used to update the progress bar by updating the scope thru a callback function..it only calls $scope.apply
			//back in the controller
			removeObject : function(filename,path,cb) {return s3RemoveObject(filename,path,cb); }
	};
			

}

]);