'use strict';
angular.module('core').factory('GlobalSettings', [
	function(){
		var _this=this;
		_this.infoEmail = 'info@flowcast.com';
		_this.profileImageHeight = '300';
		_this.profileImageWidth = '300';
		_this.applicationProfileImageHeight = '200';
		_this.applicationProfileImageWidth = '200';
		_this.ageMin = 0;
		_this.ageMax = 150;
		_this.heightMin = 0;
		_this.heightMax = 10;
		var nextPage = [];//used to pass information to next page
		_this.popNextPage = function () {
			return nextPage.pop();
		};
		_this.checkNextPage=function () {
			return nextPage.valueOf(nextPage.length-1);
		};
		_this.pushNextPage = function (page) {
			nextPage.push(page);
		};
		return _this;
	}

]);
