'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Review Schema
 */
var ReviewSchema = new Schema({
	title: {
		type: String,
		default: '',
		required: 'Please fill Review title',
		trim: true
	},

	score : Number, 

	role : { type: String //employer or employee for now ? 

	},

	description : {type : String,
	},

	casting : {type: Schema.ObjectId, ref: 'Casting'},

	completed : Boolean,

	created: {
		type: Date,
		default: Date.now
	},

	userFor : {  //reference to the user who this review is ABOUT
		type: Schema.ObjectId,
		ref: 'User'
	},

	//the fields userCreated and user are the same
	userCreated : {  //reference to the user who created this review
		type: Schema.ObjectId,
		ref: 'User'
	},

	user: {  //user who created this review
		type: Schema.ObjectId,
		ref: 'User'
	},
	reviewFor : {
		type: String,
		enum: ['for_employer', 'for_employee']
	},
});

mongoose.model('Review', ReviewSchema);
