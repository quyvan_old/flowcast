'use strict';

angular.module('castings').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Post a Casting Call', 'castings/create', 'item', '/castings/create');
		Menus.addMenuItem('topbar', 'Browse Casting Calls', 'castings', 'item', '/castings');
	}
]);
