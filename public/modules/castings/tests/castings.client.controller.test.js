'use strict';

(function() {
	// Castings Controller Spec
	describe('Castings Controller Tests', function() {
		// Initialize global variables
		var CastingsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Castings controller.
			CastingsController = $controller('CastingsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Casting object fetched from XHR', inject(function(Castings) {
			// Create sample Casting using the Castings service
			var sampleCasting = new Castings({
				name: 'New Casting'
			});

			// Create a sample Castings array that includes the new Casting
			var sampleCastings = [sampleCasting];

			// Set GET response
			$httpBackend.expectGET('castings').respond(sampleCastings);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.castings).toEqualData(sampleCastings);
		}));

		it('$scope.findOne() should create an array with one Casting object fetched from XHR using a castingId URL parameter', inject(function(Castings) {
			// Define a sample Casting object
			var sampleCasting = new Castings({
				name: 'New Casting'
			});

			// Set the URL parameter
			$stateParams.castingId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/castings\/([0-9a-fA-F]{24})$/).respond(sampleCasting);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.casting).toEqualData(sampleCasting);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Castings) {
			// Create a sample Casting object
			var sampleCastingPostData = new Castings({
				name: 'New Casting'
			});

			// Create a sample Casting response
			var sampleCastingResponse = new Castings({
				_id: '525cf20451979dea2c000001',
				name: 'New Casting'
			});

			// Fixture mock form input values
			scope.name = 'New Casting';

			// Set POST response
			$httpBackend.expectPOST('castings', sampleCastingPostData).respond(sampleCastingResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Casting was created
			expect($location.path()).toBe('/castings/' + sampleCastingResponse._id);
		}));

		it('$scope.update() should update a valid Casting', inject(function(Castings) {
			// Define a sample Casting put data
			var sampleCastingPutData = new Castings({
				_id: '525cf20451979dea2c000001',
				name: 'New Casting'
			});

			// Mock Casting in scope
			scope.casting = sampleCastingPutData;

			// Set PUT response
			$httpBackend.expectPUT(/castings\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/castings/' + sampleCastingPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid castingId and remove the Casting from the scope', inject(function(Castings) {
			// Create new Casting object
			var sampleCasting = new Castings({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Castings array and include the Casting
			scope.castings = [sampleCasting];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/castings\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleCasting);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.castings.length).toBe(0);
		}));
	});
}());