'use strict';

// Notifications controller
angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications','NotificationUtil', 'UserCheck', 
	function($scope, $stateParams, $location, Authentication, Notifications,NotificationUtil, UserCheck) {
		$scope.authentication = Authentication;
		$scope.viewnotifyfields = NotificationUtil.getViewFields();
		$scope.userCheck = UserCheck;
		//console.log($scope.viewnotifyfields);

		// Create new Notification
		$scope.create = function() {
			// Create new Notification object
			var notification = new Notifications ({
				name: this.name
			});

			// Redirect after save
			notification.$save(function(response) {
				$location.path('notifications/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Notification
		$scope.remove = function(notification) {
			if ( notification ) { 
				notification.$remove();

				for (var i in $scope.notifications) {
					if ($scope.notifications [i] === notification) {
						$scope.notifications.splice(i, 1);
					}
				}
			} else {
				$scope.notification.$remove(function() {
					$location.path('notifications');
				});
			}
		};

		// Update existing Notification
		$scope.update = function() {
			var notification = $scope.notification;

			notification.$update(function() {
				$location.path('notifications/' + notification._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Notifications
		$scope.find = function() {
			$scope.notifications = Notifications.query();
		};

		// Find existing Notification
		$scope.findOne = function() {
			$scope.notification = Notifications.get({ 
				notificationId: $stateParams.notificationId
			}, function(response){

				//console.log(response); 
				if (response.seen === false){
					response.seen = true;

					response.$update( function(response){
						//console.log('saved notification!');
						//NotificationUtil.updateNotifications(NotificationUtil.getNumNotifications()); //update the notification bar immediately ---> not really necessary
				});
				}

			});
				
		};
		$scope.ifNotAuthorizedRedirectFindOne = function() {
			$scope.notification = Notifications.get({ 
				notificationId: $stateParams.notificationId
			}, function(response){
				$scope.userCheck.redirectIfNotAuthorizedUser($scope.authentication.user, response.user);

				//console.log(response); 
				if (response.seen === false){
					response.seen = true;

					response.$update( function(response){
						//console.log('saved notification!');
						//NotificationUtil.updateNotifications(NotificationUtil.getNumNotifications()); //update the notification bar immediately ---> not really necessary
				});
				}

			});
				
		};
	}
]);
