'use strict';

angular.module('notifications').directive('previewNotification', ['UserCheck',
	function(UserCheck) {
		var directive = {};

		directive.scope = {
			notification: '=injectNotification',
			customTemplate: '@injectCustomTemplate',
			customFunction: '&injectCustomFunction'
		};

		directive.link = function (scope) {
			scope.userCheck = UserCheck;
		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/notifications/views/preview-notification.client.view.html';
		return directive;
}]);


