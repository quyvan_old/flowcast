'use strict';

angular.module('notifications').filter('unsafe', ['$sce',
	function($sce) {
		return function(input) {
			return $sce.trustAsHtml(input);
		};
	}
]);