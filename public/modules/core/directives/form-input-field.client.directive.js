'use strict';
/*
This directive is used to display/handle form inputs

Example useage:
<form-field  inject-type="text" inject-model="casting" inject-field="title">
</form-field>

You must use:
	- the type attribute which indicates which type of input is required
	- the model attribute which passes the object to be read from
	- the field attribute which indicates what field this is for

Optional attributes are:
	- the 'required' attribute which indicates if the form field is required... inject-required="true", false otherwise
	- the 'rows' | 'min' | 'max' | 'maxLength' attribute which is used when, for example, the type is "textarea" which has the attribute
	- the 'selectList' attribute which is used to hold an array for the use case type=selectionList
	- the 'placeholder' attribute which is used to pass placeholder text
*/

angular.module('core').directive('formField', //use 'form-input-field' in html to call
	function() {
		var directive = {};

		directive.scope = { 
			type: '@injectType',
			model: '=injectModel',
			field: '@injectField',
			required: '@injectRequired',
			rows: '@injectRows',
			maxLength: '@injectMaxLength',
			min: '@injectMin',
			max: '@injectMax',
			selectList: '=injectSelectList',
			placeholder: '@injectPlaceholder'

		};
		directive.link = function (scope, element, attrs){
			scope.addToList = function (input) {
				if (scope.model[scope.field].indexOf(input)!==-1) {//already in list
					scope.model[scope.field].error='Already listed.';

				}
				else if (input==='' || input===undefined){//when people forget to set a default select
					scope.model[scope.field].error='Please select an option.';

				}
				else {//not in list
					scope.model[scope.field].push(input);
					scope.model[scope.field].error='';
				}
			};
			scope.removeSelected = function(input) {
				var index = scope.model[scope.field].indexOf(input);
				if (index > -1) {
					scope.model[scope.field].splice(index, 1);
				}
			};

		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/core/views/form-field.client.view.html';
		return directive;
});
