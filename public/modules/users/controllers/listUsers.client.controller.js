'use strict';

angular.module('users').controller('ListUsersController',['$scope', '$stateParams','$resource', '$location', 'Users', 'UserObject','Authentication', 'UserCheck', '$sce', 
	function($scope, $stateParams, $resource, $location, Users, UserObject, Authentication, UserCheck, $sce) {
		$scope.authentication = Authentication;
		$scope.userCheck = UserCheck;
		$scope.$sce = $sce;
		$scope.find = function() {
			$scope.users = Users.query(function(response){ 
				$scope.pagination.start(response.length);
			});
		};
	}
]);
