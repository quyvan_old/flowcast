'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			//should be home.client.view.html not home2.client.view.html
			//home2 was just backup
			templateUrl: 'modules/core/views/home.client.view.html'
		}).
		state('about', {
			url:'/about',
			templateUrl: 'modules/core/views/about.client.view.html'
		}).
		state('contact', {
			url:'/contact',
			templateUrl: 'modules/core/views/contact.client.view.html'
		}).
		state('privacyPolicy', {
			url:'/privacypolicy',
			templateUrl: 'modules/core/views/privacypolicy.client.view.html'
		}).
		state('qstats', {
			url:'/qstats',
			templateUrl: 'modules/core/views/qstats.client.view.html'
		}).
		state('termsOfUse', {
			url:'/termsofuse',
			templateUrl: 'modules/core/views/termsofuse.client.view.html'
		});
	}
]);
