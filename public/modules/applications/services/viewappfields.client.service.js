'use strict';

angular.module('applications').factory('Viewappfields', [
	function() {
		// Viewappfields service logic
		// ...
		var create_fields =  ['name','message'];//omg why is this written in the 'application-field' file too
		var editable_fields = ['message'];
		
		var getCreateFields = function(){
			return create_fields; 
		};

		var getEditFields = function(){
			return editable_fields; 
		};


		// Public API - can have specific methods and what they return here
		return {

			createFields : function() {return getCreateFields(); }, //list of methodName : what it returns ---> usually a function 
			editFields : function() {return getEditFields(); }

		};
	}
]);
