'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Casting = mongoose.model('Casting'),
	_ = require('lodash');

/**
 * Create a Casting
 */
exports.create = function(req, res) {
	var casting = new Casting(req.body);
	casting.user = req.user;

	casting.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(casting);
		}
	});
};

/**
 * Show the current Casting
 */
exports.read = function(req, res) {
	res.jsonp(req.casting);
};

/**
 * Update a Casting
 */
exports.update = function(req, res) {
	var casting = req.casting ;

	casting = _.extend(casting , req.body);

	casting.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(casting);
		}
	});
};

/**
 * Delete an Casting
 */
exports.delete = function(req, res) {
	var casting = req.casting ;

	casting.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(casting);
		}
	});
};

/**
 * List of Castings
 */
exports.list = function(req, res) { 
	var query = {};

	//progress handler-------------
	var progress = req.param('progress');
	if (progress ==='' || progress===undefined) {
		console.log('');
	}
	else {
		query.progress = progress;
	}

	//user handler----------
	var userId = req.param('userId');
	var userFlag = false;
	if (userId===undefined) {
		console.log('');
	}
	else {
		userFlag = true;
	}

	Casting.find(query).sort('-created').populate('user', 'displayName').populate('employee').exec(function(err, castings) { // is displayName required or can we use _id instead?
		if (err) {						//must use displayName instead of ID because the data is used in the view
			return res.status(400).send({			// doesn't populate pull reference... why doesn't it show the other fields as well?
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var finalCastingList = [];
			//-------Castings created by specific user only
			if (userFlag) {
				for (var index in castings)
				{
					if (JSON.stringify(castings[index].user._id) === JSON.stringify(userId)) { //the id is stored as an object, need to make sure they are same format
						finalCastingList.push(castings[index]);
					}
				}
				res.jsonp(finalCastingList);
				return '';
			}
			//----user characteristics handler------------
			//The params are information of the current user
			// Goal is to get all castings that are applicable to user

			//set vars
			var checkListArrays = ['languages', 'portrayableEthnicity', 'skills'];
			var checkListNonArrays = ['portrayableAgeMin', 'portrayableAgeMax', 'height', 'gender', 'union'];
			var user = {};
			var checkListField;
			var fieldValue;

			//First Step - check if any param is in the checkList arrays... if so, add data to user object
			for (var checkListArraysIndex in checkListArrays) {
				checkListField = checkListArrays[checkListArraysIndex];
				fieldValue = req.param(checkListField);
				if (fieldValue !== '' && fieldValue !== undefined){
					user[checkListField] = fieldValue;
				}
			}
			//loop checks the second checkList array
			for (var checkListNonArraysIndex in checkListNonArrays) {
				checkListField = checkListNonArrays[checkListNonArraysIndex];
				fieldValue = req.param(checkListField);
				if (fieldValue !== '' && fieldValue !== undefined){
					user[checkListField] = req.param(checkListField);
				}
			}

			//Second Step - If filters is not true, it means the user searched for all castings
			if (req.param('filters') !== 'filter' ) { //search for all castings
				res.jsonp(castings);
				return '';
			}


			//function to check if a field in userArray is in all values of castingArray
			var isUsersArrayInCastingsArray = function (usersArray, castingsArray) {
				if (castingsArray ===undefined) {
					return true;
				}
				if (!castingsArray.length || castingsArray === '') {
					return true;
				}
				if (usersArray === undefined || usersArray === ''){ 
					return false;
				}
				if (typeof usersArray === 'string') {//if considered as string, turn into single value in an array
					var usersNewArray = [];
					usersNewArray.push(usersArray);
					usersArray=usersNewArray;

				}

				var castingsCompareObject;
				var usersCompareObject;
				var compareFlag;
				var castingsArrayIndex = 0;

				while  (castingsArrayIndex < castingsArray.length) { //loop through the castings array
					castingsCompareObject = castingsArray[castingsArrayIndex];
					compareFlag = false;
					for (var usersArrayIndex in usersArray) { // loop through the users array
						usersCompareObject = usersArray[usersArrayIndex];
						if (castingsCompareObject === usersCompareObject) {
							compareFlag = true;
							break;
						}
					}
					if (compareFlag ===false) {
						return false;
					}
					castingsArrayIndex++;
				}
				return true;

			};

			var finalCastingFlag;

			//loop through all checkLists
			for (var castingsIndex in castings) {
				finalCastingFlag = true;
				if (finalCastingFlag===true) {
					for (checkListArraysIndex in checkListArrays) {//loop through checkListArrays
						checkListField = checkListArrays[checkListArraysIndex];
						if (!isUsersArrayInCastingsArray(user[checkListField], castings[castingsIndex][checkListField])) {
							finalCastingFlag = false;
							break;
						}
					}
				}

				if (finalCastingFlag === true) {//if it passed first checkListArray, test out the nonArrays list
					for (checkListNonArraysIndex in checkListNonArrays) {
						checkListField = checkListNonArrays[checkListNonArraysIndex];
						if (checkListField==='portrayableAgeMin' || checkListField==='portrayableAgeMax') {
							var castingAgeMin = castings[castingsIndex].portrayableAgeMin;
							var castingAgeMax = castings[castingsIndex].portrayableAgeMax;
							var userAgeMin = user.portrayableAgeMin;
							var userAgeMax = user.portrayableAgeMax;
							if (castingAgeMin !== undefined) { // casting MIN exists
								if (userAgeMax === undefined) { // user MAX does NOT exist
									finalCastingFlag=false;
									break;
								}
								if (castingAgeMin > userAgeMax) {// not in range
									finalCastingFlag=false;
									break;
								}
							}
							if (castingAgeMax !== undefined) {//casting MAX exists
								if (userAgeMin === undefined) {//user min does NOT exist
									finalCastingFlag=false;
									break;
								}
								if (castingAgeMax < userAgeMin) {//not in range
									finalCastingFlag=false;
									break;
								}
							}
						}
						else if (checkListField==='height') {
							var castingHeightMin = castings[castingsIndex].heightMin;
							var castingHeightMax = castings[castingsIndex].heightMax;
							var userHeight = user.height;
							if (castingHeightMin !== undefined && castingHeightMax !== undefined) {
								if (userHeight < castingHeightMin || userHeight > castingHeightMax)//if not in range
								{
									finalCastingFlag = false;
									break;

								}
							}
						}
						else if (checkListField==='union') {
							var castingUnion = castings[castingsIndex].union;
							var userUnion = user.union;

							if (castingUnion === 'Union') {
								if (userUnion === 'Non-Union' || userUnion===undefined) {
									finalCastingFlag=false;
									break;
								}
							}
							else if (castingUnion==='Non-Union') {
								if (userUnion !== 'Non-Union' && userUnion!==undefined) {
									finalCastingFlag=false;
									break;
								}
							}
							//else castingUnion==='Union and Non-Union' ... it's good
						}
						else if (checkListField==='gender') {
							var castingGender = castings[castingsIndex].gender;
							var userGender = user.gender;
							if (castingGender !== undefined) {
								if (userGender === undefined ) {
									finalCastingFlag = false;
									break;
								}
								if (castingGender !== userGender) {
									finalCastingFlag=false;
									break;
								}
							}
						}
						
					}
				}
				if (finalCastingFlag===true){//check if closed
					if (castings[castingsIndex].progress!=='hiring') {
						finalCastingFlag=false;
					}

				}
				if (finalCastingFlag ===true){//if pass both lists, it's valid
					finalCastingList.push(castings[castingsIndex]);
				}
			}
			res.jsonp(finalCastingList);
		}
	});


};

/**
 * Casting middleware
 */
exports.castingByID = function(req, res, next, id) { 

	Casting.findById(id).populate('user').populate('employee').populate('review.for_employer').populate('review.for_employee').exec(function(err, casting) {  //what does the populate command do ? A: Fills in references
		if (err) return next(err);
		if (! casting) return next(new Error('Failed to load Casting ' + id));
		req.casting = casting ;
		next();
	});
};


exports.haspublicAuthorization = function (req,res,next){
	if (!req.user) { //make sure the user actually exists? Should probably check user._id or something b/c user is always there ?
		return res.status(403).send('Please create an account or sign in to continue');
	}
	next();

};


/**
 * Casting authorization middleware
 */
 //this doesn't seem to work... it's not giving me a response error - quy
exports.hasAuthorization = function(req, res, next) {
	if (req.casting.employer && req.user)
	{
		if (req.casting.employer._id !== req.user._id) {
			return res.status(403).send('User is not authorized');
		}
	}
	next();
};
