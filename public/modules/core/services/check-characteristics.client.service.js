'use strict';
angular.module('core').factory('CheckCharacteristics', [
	function(){
		var _this=this;

		_this.characteristicsNotEqualAlert = function (message) {
			alert(message); 
		};

		_this.castingAndUserMatch = function (casting, user){ // IF YOU MODIFY THIS, modify the castings.server.controller as well... exports.list
			var checkList = ['languages', 'skills', 'portrayableEthnicity'];
			var castingField;
			var castingValue;
			var userField;
			var userValue;
			var sameFlag;
			if (casting !== undefined ) {
				if (user === undefined) {
					return false;
				}
			}
			for (var checkListIndex in checkList) {
				castingField = casting[checkList[checkListIndex]];
				userField = user[checkList[checkListIndex]];

				/*
					The following sections goal is to compare the casting's array and the user's array and return false if not equal
				*/
				if (castingField.length) { 
					if (userField.length) {// preliminary check if the user's array is empty, but the casting's requires it

						for (var castingFieldIndex in castingField) { //loop through the casting's array
							castingValue = castingField[castingFieldIndex];
							sameFlag = false;

							for (var userFieldIndex in userField) {//loop through the user's array
								userValue = userField[userFieldIndex];

								if (castingValue === userValue) {//if the casting's is in user's... go on to next casting's
									sameFlag = true;
									break;
								}
							}
							if (sameFlag === false ) { // if it loops through user's without finding match.. return false
								return false;
							}
						}
					}
					else { //if defined casting's but user's not defined
						return false;
					}
				}

			}
			//check age and gender
			return ( _this.isUserUnionInCastingUnion(user,casting) && 
			_this.isUserInCastingAgeRange(user,casting) && 
			_this.isUserGenderSameAsCasting(user,casting) && 
			_this.isUserInHeightRange(user, casting));
		};

		//function returns false if user is not in range... returns true if in range
		_this.isUserInCastingAgeRange = function (user, casting) {
			if (casting.portrayableAgeMin !== undefined) { // casting MIN exists
				if (user.portrayableAgeMax === undefined) { // user MAX does NOT exist
					return false;
				}
				if (casting.portrayableAgeMin > user.portrayableAgeMax) {// not in range
					return false;
				}
			}
			if (casting.portrayableAgeMax !== undefined) {//casting MAX exists
				if (user.portrayableAgeMin === undefined) {//user min does NOT exist
					return false;
				}
				if (casting.portrayableAgeMax < user.portrayableAgeMin) {//not in range
					return false;
				}
			}
			return true;//if passes all checks

		};
		//function returns false if user is not in range... returns true if in range
		_this.isUserInHeightRange = function (user, casting) {
			if (casting.heightMin !== undefined && casting.heightMax!== undefined) {
				if (user.height === undefined) { // user height does NOT exist
					return false;
				}
				if (user.height < casting.heightMin || user.height > casting.heightMax) {// not in range
					return false;
				}
			}
			return true;//if passes all checks

		};
		_this.isUserGenderSameAsCasting = function (user, casting) {
			if (casting.gender === undefined) {
				return true;

			}
			if (user.gender === undefined) {
				return false;
			}
			if (casting.gender === user.gender) {
				return true;
			}
			return false;
		};
		_this.isUserUnionInCastingUnion = function (user, casting) {
			if (casting.union === undefined || casting.union==='Union and Non-union') {
				return true;
			}
			if (casting.union === 'Union') {
				if (user.union === 'Non-Union' || user.union===undefined) {
					return false;
				}
				return true;
			}
			else if (casting.union==='Non-Union') {
				if (user.union === 'Non-Union') {
					return true;
				}
				return false;
			}
			return true;
		};
		return _this;
	}

]);
