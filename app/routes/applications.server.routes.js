'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var applications = require('../../app/controllers/applications.server.controller');

	// Applications Routes
	app.route('/applications')
		.get(applications.list)
		.post(users.requiresLogin, applications.create);

	app.route('/applications/:applicationId')
		.get(applications.read)
		.put(users.requiresLogin, applications.haspublicAuthorization, applications.update)
		.delete(users.requiresLogin, applications.hasAuthorization, applications.delete);

	app.route('/applications/:castingId') //this is not even being used??
		.get(applications.listByCastingID);

	// Finish by binding the Application middleware
	app.param('applicationId', applications.applicationByID); //JUST USES THE applicationId in the middleware function which adds to the request ? 
	// app.param('castingIdApp', applications.applicationsByCastingID); YOU CAN'T ROUTE IN EXPRESS BASED ON QUERY PARAMETERS...SOOOO GAY
	//bro u mad?
};
