'use strict';


angular.module('users').factory('UserObject', ['$resource',
	function($resource) {
		return $resource('users/profile/:userUrl', { userUrl: '@userUrl'   
		},{ 
			update: {
				method: 'GET'
			}
		});
	}
]);
