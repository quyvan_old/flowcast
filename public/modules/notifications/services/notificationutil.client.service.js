'use strict';

angular.module('notifications').factory('NotificationUtil', ['Notifications','Menus',
	function(Notifications,Menus) {
		// Viewappfields service logic
		// ...
		var create_fields =  ['name','description'];
		var editable_fields = ['description'];
		var view_fields = ['name','message']; 	
		var num_notifications = 0; //number of notifications that the user currently has 
		
		//currently does not work ---> need to fix later
	    var notification_update_function = function(curr) {
	    	//console.log('in the service function...');
	    	var menu = Menus.getMenu('topbar');
	    	//console.log(menu); //does this work?
			var notifications = Notifications.query({}, function(response){
				//console.log(response);
				var notif_counter = 0; 
				for (var i = 0; i < response.length; i++){
					if (response[i].seen === false){
						notif_counter ++;
					}
				}

				var old_notification_num = curr; //not sure if this works
				if (old_notification_num !== notif_counter && notif_counter!== 0){ //update the menu 
					if (notif_counter !== 0){
						menu[2].title = 'Notifications' + '(' + notif_counter + ')';
					}
					else{
						menu[2].title = 'Notifications';
					}
					
				}

				//save the notification number for the next time
				//not sure if this will work
			//	this.num_notifications = curr;
				//console.log('there are currently '+notif_counter+ ' notifications');

			});	    	
	    };

		var createNotify = function(){
			//console.log('in the create notify function');
			return 3; 
		};

		var createHTMLStringUser = function(text,url){ //link to user profile 
			var result = '<a href = \"/#!/users/profile/' + url + '\">' + text + '</a>';
			//console.log('in the html user function');
			return result; 
		};

		var createHTMLStringApplication = function(text,id){ //link to a specific application

			var result = '<a href = \"/#!/applications/' + id + '\">' + text + '</a>';
			//console.log('in the html applicaiton function');

			return result; 
		};


		var createHTMLStringReview = function(text,id){ //link to a specific review

			var result = '<a href = \"/#!/reviews/' + id + '\">' + text + '</a>';
			//console.log('in the html applicaiton function');

			return result; 
		};


		var createHTMLStringCasting = function(text,id){
			//console.log('in the create html casting ref function');
			var result = '<a href = \"/#!/castings/' + id + '\">' + text + '</a>';
			return result; 
		};		

		var getFieldsForView = function(){
			return view_fields; 
		};


		// Public API - can have specific methods and what they return here
		return {

			createNotification : function() {return createNotify(); }, //list of methodName : what it returns ---> usually a function 
			createHTMLStringUser : function(text,url) {return createHTMLStringUser(text,url);},
			createHTMLStringApplication : function(text,id) {return createHTMLStringApplication(text,id);},
			createHTMLStringCasting : function(text,id){return createHTMLStringCasting(text,id);},
			createHTMLStringReview : function(text,id){return createHTMLStringReview(text,id);},
			getViewFields : function(){return getFieldsForView();},
			getNumNotifications : function(){return num_notifications;},
			setNumNotifications: function(updated_num){ num_notifications = updated_num;},
			updateNotifications: function(num){notification_update_function(num);} //currently does not work...fix later
};
	}

]);