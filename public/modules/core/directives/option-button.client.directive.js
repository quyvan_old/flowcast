'use strict';
/*
This directive is used to display a button

Example useage:

<option-button inject-button-type="href" inject-href="/#!/signin" inject-text="Apply Now">
</option-button>

You must use:
	- the buttonType attribute which is used to indicate the buttons function... ie click vs href
		- the href attribute... which is used only when type="href"
		-----------
		- the clickFunction attribute... which is holds the function that will init when clicked
	- the injectText attribute... which is the text that is to be displayed

Other optional attributes are:
	- the showIf attribute which indicates whether the button should show... uses ng-if on the statement
	- the buttonClassExtension attribute  which is used to pass in other classes... such as "btn-success"
	- the appendIcon attribute is used to pass an icon in the front of button
	- the prependIcon attribute is used to pass an icon in the back of button
*/

angular.module('core').directive('optionButton', //use 'option-button' in html to call
	function() {
		var directive = {};

		directive.scope = {
			href: '@injectHref', //use hyphenated form... ie 'inject-Href in html to call
			clickFunction: '&injectClickFunction',
			showIf: '=injectShowIf',
			buttonType: '@injectButtonType',
			buttonClassExtension: '@injectButtonClassExtension',
			appendIcon: '@injectAppendIcon',
			prependIcon: '@injectPrependIcon',
			text: '@injectText'
		};
		directive.link = function (scope, element, attrs){

		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/core/views/option-button.client.view.html';
		return directive;
});
