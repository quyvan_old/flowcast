'use strict';

angular.module('applications').directive('previewApplication',['CheckIfEmployed',
	function(CheckIfEmployed) {
		var directive = {};

		directive.scope = {
			application: '=injectApplication',
			viewType: '=injectViewType',
			list: '=injectList',
			customTemplate: '@injectCustomTemplate'
		};
		directive.link= function (scope) {
			scope.checkIfEmployed = CheckIfEmployed.checkIfEmployed();
		};
		directive.restrict = 'E';
		directive.templateUrl = 'modules/applications/views/preview-application.client.view.html';
		return directive;
}]);
