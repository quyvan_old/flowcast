'use strict';
angular.module('core').factory('YoutubeHandler', [
	function(){
		var _this=this;
		_this.getYoutubeUrlId = function (youtubeUrl) {
			var youtubeUrlStringArray = youtubeUrl.split('https://www.youtube.com/watch?v=');
			if (youtubeUrlStringArray.length===1) {//if it didnt parse
				//try if they used shared link .... http://youtu.be/E1oZhEIrer4
				youtubeUrlStringArray = youtubeUrl.split('http://youtu.be/');
			}
			var youtubeId=youtubeUrlStringArray[1];
			return youtubeId;
		};
		return _this;
	}

]);
