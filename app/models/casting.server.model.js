'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Casting Schema
 */
var CastingSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},

	progress:{ 
		type: String,
		enum: ['hiring', 'inprogress', 'closed']
		//hiring = not accepted an application
		//inprogress = accepted an application, but the casting complete button has not been pressed
		//closed = casting is over
	},

	//holds a url thats a link a picture the user can upload with their casting (such as a movie poster hence the url)
	movie_img_url:{ 
		type: String
	},

	user: {      
		type: Schema.ObjectId,
		ref: 'User'
	},
	employee:{ 
		type: Schema.ObjectId,
		ref: 'User'
	},
	address:{ 
		type: String
	},
	pay:{ 
		type: Number
	},
	review: {
		for_employer: {
			type: Schema.ObjectId,
			ref: 'Review'
		},
		for_employee: {
			type: Schema.ObjectId,
			ref: 'Review'
		}
	},
	portrayableAgeMin: {
		type: Number
	},
	portrayableAgeMax: {
		type: Number
	},
	applications : [{
		type: Schema.ObjectId, 
		ref : 'Application'
	}],  
	title:{ 
		type: String
	},
	description:{ 
		type: String
	},
	payment: {
		type: String,
	},
	union: {
		type: String,
	},
	portrayableEthnicity: [{
		type: String
	}],
	languages: [{
		type: String,
	}],
	heightMin: {
		type: Number
	},
	heightMax: {
		type: Number
	},
	gender: {
		type: String,
	},
	skills: [{
		type: String
	}],
	youtubeUrl: {
		type: String
	},
});

mongoose.model('Casting', CastingSchema);
