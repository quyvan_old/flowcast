'use strict';
angular.module('core').filter('camelCaseToHuman', function() {
	return function(input) {
		return input.replace(/([A-Z])/g, ' $1');
	};
});
